﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using InsightHub.Data.Entities;
using System;

namespace InsightHub.Data.DatabaseSeeder
{
    public static class ModelBuilderExtension
    {
        //Roles
        private static readonly int AdminRole_Id = 1;
        private static readonly int AuthorRole_Id = 2;
        private static readonly int Customer_Id = 3;

        //Industries
        private static readonly int IT_Id = 1;
        private static readonly int HealthCare_Id = 2;
        private static readonly int Finance_Id = 3;
        private static readonly int Defence_Id = 4;
        private static readonly int Gaming_Id = 5;
        private static readonly int Education_Id = 6;
        private static readonly int Energy_Utilities_Id = 7;
        private static readonly int Retail_Id = 8;
        private static readonly int Electric_Utilities_Id = 9;
        private static readonly int Telephone_Utilities_id = 10;
        private static readonly int Mortgage_Bankers_Brokers_Id = 11;
        private static readonly int Air_Transport_Id = 12;
        private static readonly int Business_Franchises_id = 13;
        private static readonly int Life_Sciences_id = 14;
        private static readonly int Global_Commercial_Banks_id = 15;
        private static readonly int Pension_Funds_id = 16;
        private static readonly int Commercial_Real_Estate_id = 17;
        private static readonly int Tourism_id = 18;
        private static readonly int Consumer_Goods_Services_id = 19;
        private static readonly int Infrastructure_Constractors_id = 20;

        //Reports
        private static readonly int WhatIsNewInJava8_Id = 1;
        private static readonly int WC3IsBeingSavedFromPad_Id = 2;
        private static readonly int BusinessAnalyst_Id = 3;
        private static readonly int Distance_Learning_Id = 4;
        private static readonly int HowImportantIsVitaminA_Id = 5;
        private static readonly int HowImportantIsVitaminC_Id = 6;
        private static readonly int HowImportantIsVitaminD_Id = 7;
        private static readonly int Environmental_impact_Id = 8;
        private static readonly int Electric_utilities_Id = 9;
        private static readonly int Energy_Industry_Id = 10;
        private static readonly int Retailing_in_antiquity_Id = 11;

        //Users
        private static readonly int Yani_Id = 1;
        private static readonly int Pesho_Id = 2;
        private static readonly int Uther_Id = 3;
        private static readonly int Baal_Id = 4;
        private static readonly int Mephisto_Id = 5;
        private static readonly int Uther2_Id = 6;
        private static readonly int Admin_Id = 7;
        private static readonly int Radko_Id = 8;
        private static readonly int Kiro_Id = 9;
        private static readonly int Edward_Id = 10;

        public static void Seeder(this ModelBuilder builder)
        {
            builder.Entity<Industry>().HasData
           (
                new Industry
                {
                    Id = IT_Id,
                    ImgUrl = "/IT.jpg",
                    Name = "IT",
                },
                new Industry
                {
                    Id = HealthCare_Id,
                    ImgUrl = "/Health Care.jpg",
                    Name = "Health Care",
                },
                new Industry
                {
                    Id = Finance_Id,
                    ImgUrl = "/Finance.jpg",
                    Name = "Finance",
                },
                new Industry
                {
                    Id = Defence_Id,
                    ImgUrl = "/Defence.jpg",
                    Name = "Defence",
                    IsDeleted = true
                },
                new Industry
                {
                    Id = Gaming_Id,
                    ImgUrl = "/Gaming.jpg",
                    Name = "Gaming",
                },
                new Industry
                {
                    Id = Education_Id,
                    ImgUrl = "/Education.jpg",
                    Name = "Education",
                },
                new Industry

                {
                    Id = Energy_Utilities_Id,
                    ImgUrl = "/Energy Utilities.jpg",
                    Name = "Energy Utilities",
                },
                new Industry

                {
                    Id = Retail_Id,
                    ImgUrl = "/Retail.jpg",
                    Name = "Retail",
                },
                new Industry

                {
                    Id = Electric_Utilities_Id,
                    ImgUrl = "/Electric Utilities.jpg",
                    Name = "Electric Utilities",
                },
                new Industry
                {
                    Id = Telephone_Utilities_id,
                    ImgUrl = "/Telephone Utilities.jpg",
                    Name = "Telephone Utilities",
                },
                new Industry

                {
                    Id = Mortgage_Bankers_Brokers_Id,
                    ImgUrl = "/Mortgage Bankers & Brokers.jpg",
                    Name = "Mortgage Bankers & Brokers",
                },
                new Industry
                {
                    Id = Air_Transport_Id,
                    ImgUrl = "/Air Transport.jpg",
                    Name = "Air Transport",
                },
                new Industry
                {
                    Id = Business_Franchises_id,
                    ImgUrl = "/Business Franchises.jpg",
                    Name = "Business Franchises",
                },
                new Industry
                {
                    Id = Life_Sciences_id,
                    ImgUrl = "/Life Sciences.jpg",
                    Name = "Life Sciences",
                },
                new Industry
                {
                    Id = Global_Commercial_Banks_id,
                    ImgUrl = "/Global Commercial Banks.jpg",
                    Name = "Global Commercial Banks",
                },
                new Industry

                {
                    Id = Pension_Funds_id,
                    ImgUrl = "/Pension Funds.jpg",
                    Name = "Pension Funds",
                },
                new Industry

                {
                    Id = Commercial_Real_Estate_id,
                    ImgUrl = "/Commercial Real Estate.jpg",
                    Name = "Commercial Real Estate",
                },
                new Industry

                {
                    Id = Tourism_id,
                    ImgUrl = "/Tourism.jpg",
                    Name = "Tourism",
                },
                new Industry
                {
                    Id = Consumer_Goods_Services_id,
                    ImgUrl = "/Consumer Goods Services.jpg",
                    Name = "Consumer Goods Services",
                },
                new Industry
                {
                    Id = Infrastructure_Constractors_id,
                    ImgUrl = "/Infrastructure Constractors.jpg",
                    Name = "Infrastructure Constractors",
                }
           );

            builder.Entity<Tag>().HasData
               (
                new Tag
                {
                    Id = 1,
                    Name = "reports",
                },

                new Tag
                {
                    Id = 2,
                    Name = "games",
                },

                new Tag
                {
                    Id = 3,
                    Name = "insight",
                },
                new Tag
                {
                    Id = 4,
                    Name = "analysis",
                },
                new Tag
                {
                    Id = 5,
                    Name = "business",
                },
                new Tag
                {
                    Id = 6,
                    Name = "banks",
                },
                new Tag
                {
                    Id = 7,
                    Name = "money",
                },
                new Tag
                {
                    Id = 8,
                    Name = "trends",
                },
                new Tag
                {
                    Id = 9,
                    Name = "learning",
                },
                new Tag
                {
                    Id = 10,
                    Name = "retail",
                },
                new Tag
                {
                    Id = 11,
                    Name = "energy",
                },
                new Tag
                {
                    Id = 12,
                    Name = "marketing",
                },

                new Tag
                {
                    Id = 13,
                    Name = "featured",
                },

                new Tag
                {
                    Id = 14,
                    Name = "new",
                },
                new Tag
                {
                    Id = 15,
                    Name = "popular",
                },
                new Tag
                {
                    Id = 16,
                    Name = "data",
                },
                new Tag
                {
                    Id = 17,
                    Name = "services",
                },
                new Tag
                {
                    Id = 18,
                    Name = "technology",
                },
                new Tag
                {
                    Id = 19,
                    Name = "health",
                },
                new Tag
                {
                    Id = 20,
                    Name = "learning",
                },
                new Tag
                {
                    Id = 21,
                    Name = "reach",
                },
                new Tag
                {
                    Id = 22,
                    Name = "develop",
                }
          );

            builder.Entity<ReportTags>().HasData
               (
                new ReportTags
                {
                    ReportId = IT_Id,
                    TagId = 3,
                },
                new ReportTags
                {
                    ReportId = IT_Id,
                    TagId = 1,
                }
                );

            builder.Entity<ReportDownloaders>().HasData
              (
                new ReportDownloaders
                {
                    DownloaderId = Radko_Id,
                    ReportId = WhatIsNewInJava8_Id,
                    TimesDownloaded = 357
                },
                new ReportDownloaders
                {
                    DownloaderId = Kiro_Id,
                    ReportId = HowImportantIsVitaminA_Id,
                    TimesDownloaded = 59
                },
                new ReportDownloaders
                {
                    DownloaderId = Edward_Id,
                    ReportId = WC3IsBeingSavedFromPad_Id,
                    TimesDownloaded = 249
                },
                new ReportDownloaders
                {
                    DownloaderId = Radko_Id,
                    ReportId = HowImportantIsVitaminC_Id,
                    TimesDownloaded = 112
                },
                new ReportDownloaders
                {
                    DownloaderId = Kiro_Id,
                    ReportId = HowImportantIsVitaminD_Id,
                    TimesDownloaded = 77
                },
                new ReportDownloaders
                {
                    DownloaderId = Edward_Id,
                    ReportId = BusinessAnalyst_Id,
                    TimesDownloaded = 189
                },
                new ReportDownloaders
                {
                    DownloaderId = Radko_Id,
                    ReportId = Distance_Learning_Id,
                    TimesDownloaded = 199
                },
                new ReportDownloaders
                {
                    DownloaderId = Kiro_Id,
                    ReportId = Environmental_impact_Id,
                    TimesDownloaded = 111
                },
                new ReportDownloaders
                {
                    DownloaderId = Radko_Id,
                    ReportId = Retailing_in_antiquity_Id,
                    TimesDownloaded = 137
                },
                new ReportDownloaders
                {
                    DownloaderId = Kiro_Id,
                    ReportId = Energy_Industry_Id,
                    TimesDownloaded = 99
                }
               );

            builder.Entity<Role>().HasData
            (
                new Role
                {
                    Id = AdminRole_Id,
                    Name = "Admin",
                    NormalizedName = "ADMIN"
                },
                new Role
                {
                    Id = AuthorRole_Id,
                    Name = "Author",
                    NormalizedName = "AUTHOR"
                },
                new Role
                {
                    Id = Customer_Id,
                    Name = "Customer",
                    NormalizedName = "CUSTOMER"
                }
            );

            builder.Entity<Report>().HasData
           (
                new Report
                {
                    Id = WhatIsNewInJava8_Id,
                    Name = "What is new in Java 8?",
                    Summary = "Here's everything you wanted to know about what Java 8 brought to development, with a special focus on functional programming.",
                    Description = "The major portion of Java 8 was targeted toward supporting functional programming. Let's explore and understand what functional " +
                    "programming means and how it is useful and is applied in Java. Functional programming is a programming paradigm that dictates a different algorithmic " +
                    "way to think about problems and program solutions for them.T contrast it with object - oriented programming, in OOP, the primary abstraction is Class / Object, whereas " +
                    "in functional programming the primary abstraction is a function. As in OOP, objects form the building blocks for computation.Similarly, in functional programming, functions " +
                    "form the building blocks for computation.",
                    AuthorId = Pesho_Id,
                    IndustryId = IT_Id,
                },
                new Report
                {
                    Id = WC3IsBeingSavedFromPad_Id,
                    Summary = "W3Champions is a side ladder for the Warcraft 3 Reforged Battle.net ladder. You can play directly in WC3 and queue against opponents with the same skill level.",
                    Name = "WC3 is being saved from Pad!",
                    Description = "W3Champions is not the first project Pad has developed for the scene. Last year, he launched W3Booster, an overlay for streamers and " +
                    "W3Arena back in 2012. I can hardly downplay the impact both of these projects have had. W3Arena practically saved the entire scene when the official " +
                    "servers were riddled with hackers and sky-high ping. W3Booster revolutionized the experience for stream viewers, making the game much more spectator friendly." +
                    " Now, almost every Twitch streamer uses it and even in Chinese Douyu streams it is gaining popularity.",
                    AuthorId = Yani_Id,
                    IndustryId = Gaming_Id,
                },
                new Report
                {
                    Id = HowImportantIsVitaminA_Id,
                    Name = "How important is vitamin A?",
                    Summary = "Vitamin A: Benefits, Deficiency, Toxicity and More",
                    Description = "Though vitamin A is often considered a singular nutrient, it’s really the name for a group of fat-soluble compounds, including retinol, retinal and retinyl esters (1Trusted Source). " +
                    "There are two forms of vitamin A found in food. Preformed vitamin A — retinol and retinyl esters — occurs exclusively in animal products, such as dairy, liver and fish, while provitamin A carotenoids are abundant " +
                    "in plant foods like fruits, vegetables and oils(2Trusted Source). To use them, your body must convert both forms of vitamin A to retinal and retinoic acid, the active forms of the vitamin. Because vitamin A is fat " +
                    "soluble, it’s stored in body tissue for later use. Most of the vitamin A in your body is kept in your liver in the form of retinyl esters(3Trusted Source). These esters are then broken down into all - trans - retinol, which" +
                    " binds to retinol binding protein(RBP).It then enters your bloodstream, at which point your body can use it(4Trusted Source).",
                    AuthorId = Mephisto_Id,
                    IndustryId = HealthCare_Id,
                },
                new Report
                {
                    Id = HowImportantIsVitaminC_Id,
                    Name = "How important is vitamin C?",
                    Summary = "Vitamin C is a vital nutrient for health. It helps form and maintain bones, skin, and blood vessels. It occurs naturally in " +
                    "some foods, especially, fruit and vegetables. Supplements are also available.",
                    Description = "Vitamins, including vitamin C, are organic compounds. An organic compound is one that exists in living things and contains the elements carbon and oxygen. Vitamin C is water soluble, and the " +
                    "body does not store it.To maintain adequate levels of vitamin C,  humans need a daily intake of food that contains it. Vitamin C plays an important role in a number of bodily functions including the production of " +
                    "collagen, L - carnitine, and some neurotransmitters.It helps metabolize proteins and its antioxidant activity may reduce the risk of some cancers.Collagen, which vitamin C helps produce, is the main component of connective " +
                    "tissue and the most abundant protein in mammals.Between 1 and 2 % of muscle tissue is collagen.It is a vital component in fibrous tissues such as:",
                    AuthorId = Mephisto_Id,
                    IndustryId = HealthCare_Id,
                },
                new Report
                {
                    Id = HowImportantIsVitaminD_Id,
                    Name = "How important is vitamin D?",
                    Summary = "Vitamin D and your health: Breaking old rules, raising new hopes",
                    Description = "Vitamin D's best-known role is to keep bones healthy by increasing the intestinal absorption of calcium. Without enough vitamin D, the body can only absorb 10% to 15% of dietary calcium, but 30% to 40% absorption " +
                    "is the rule when vitamin reserves are normal. A lack of vitamin D in children causes rickets; in adults, it causes osteomalacia. Both bone diseases are now rare in the United States, but another is on the rise — osteoporosis, " +
                    "the \"thin bone\" disease that leads to fractures and spinal deformities. Low levels of vitamin D lead to low bone calcium stores, increasing the risk of fractures.If vitamin D did nothing more than protect bones, it would still " +
                    "be essential.But researchers have begun to accumulate evidence that it may do much more. In fact, many of the body's tissues contain vitamin D receptors, proteins that bind to vitamin D. In the intestines, the receptors " +
                    "capture vitamin D, enabling efficient calcium absorption. But similar receptors are also present in many other organs, from the prostate to the heart, blood vessels, muscles, and endocrine glands. And work in progress suggests" +
                    "that good things happen when vitamin D binds to these receptors. The main requirement is to have enough vitamin D, but many Americans don't.",
                    AuthorId = Mephisto_Id,
                    IndustryId = HealthCare_Id,
                },
                new Report
                {
                    Id = BusinessAnalyst_Id,
                    Name = "What is a business analyst?",
                    Summary = "Business analyst help guide businesses in improving processes, products, services and software through data analysis.",
                    Description = "Business analysts (BAs) are responsible for bridging the gap between IT and the business using data analytics to assess processes, determine requirements and deliver data-driven recommendations and reports to executives " +
                       "and stakeholders. BAs engage with business leaders and users to understand how data - driven changes to process, products,services, software and hardware can improve efficiencies and add value.They must articulate those ideas but also" +
                       " balance them against what’s technologically feasible and financially and functionally reasonable.Depending on the role, you might work with data sets to improve products, hardware, tools, software, services or process. The International " +
                       "Institute of Business Analysis (IIBA), a nonprofit professional association, considers the business analyst “an agent of change,” writing that business analysis “is a disciplined approach for introducing and managing change to organizations, " +
                       "whether they are for-profit businesses, governments, or non-profits.”",
                    AuthorId = Pesho_Id,
                    IndustryId = Finance_Id,
                },
                new Report
                {
                    Id = Distance_Learning_Id,
                    Name = "Distance Learning",
                    Summary = "4 Best Practices for Distance Learning to Support Students Who Learn and Think Differently.",
                    Description = "With distance learning, schools and teachers face a whole new set of challenges as they aim to make learning equitable in this new environment. As a classroom teacher, you might be wondering what you can do to meet the needs of " +
                      "students with learning and thinking differences, like dyslexia and ADHD. You might be asking yourself, How can I support students who struggle with staying focused ? How can I provide accommodations for students with special education needs ? How " +
                      "can I help students who are falling behind ? Since this is a new learning environment, teachers have to rethink how to meet students' needs. You might notice some students who had been doing well are now falling behind. They might be struggling with" +
                      " social and emotional issues related to the coronavirus pandemic. Or they might be struggling to adjust to distance learning. (You might also notice that some students who struggled in the classroom are thriving in a distance learning environment" +
                      " where they have more flexibility.)",
                    AuthorId = Yani_Id,
                    IndustryId = Education_Id,
                },
                new Report
                {
                    Id = Environmental_impact_Id,
                    Name = "Environmental impact of the energy industry",
                    Summary = "Life cycle and environmental footprint impacts",
                    Description = "All forms of electricity generation have an environmental impact on our air, water and land, but it varies. Of the total energy consumed in the United States, about 40% is used to generate electricity, making electricity use an important " +
                    "part of each person’s environmental footprint. Producing and using electricity more efficiently reduces both the amount of fuel needed to generate electricity and the amount of greenhouse gases and other air pollution emitted as a result. Electricity from renewable " +
                    "resources such as solar, geothermal, and wind generally does not contribute to climate change or local air pollution since no fuels are combusted. The emissions caused by electricity generation vary across the country due to many factors, including:How much electricity is " +
                    "generated,Electricity generation technologies used, and Air pollution control devices used Use EPA's household carbon footprint calculator to estimate your household's annual emissions and find ways you can cut emissions.Use Power Profiler to generate a report about the " +
                    "environmental impacts of electricity generation in your area of the United States.All you need is your zip code.Power Profiler takes about five minutes to use.For more in-depth information, visit the Emissions & Generation Resource Integrated Database(eGRID), a comprehensive" +
                    " source of data on the environmental characteristics of almost all electric power generated in the United States.",
                    AuthorId = Baal_Id,
                    IndustryId = Energy_Utilities_Id,
                },
                new Report
                {
                    Id = Retailing_in_antiquity_Id,
                    Name = "Retailing in antiquity",
                    Summary = "Retail is the process of selling consumer goods or services to customers through multiple channels of distribution to earn a profit.",
                    Description = "Retail markets have existed since ancient times. Archaeological evidence for trade, probably involving barter systems, dates back more than 10,000 years. As civilizations grew, barter was replaced with retail trade involving coinage. " +
                    "Selling and buying are thought to have emerged in Asia Minor (modern Turkey) in around the 7th-millennium BCE.[5] Gharipour points to evidence of primitive shops and trade centers in Sialk Hills in Kashan (6000 BCE), Catalk Huyuk in modern-day Turkey " +
                    "(7,500–5,700 BCE), Jericho (2600 BCE) and Susa (4000 BCE).[6] Open air, public markets were known in ancient Babylonia, Assyria, Phoenicia, and Egypt. These markets typically occupied a place in the town's center. Surrounding the market, skilled artisans," +
                    " such as metal-workers and leather workers, occupied permanent premises in alleys that led to the open market-place. These artisans may have sold wares directly from their premises, but also prepared goods for sale on market days.[7] In ancient Greece markets " +
                    "operated within the agora, an open space where, on market days, goods were displayed on mats or temporary stalls.[8] In ancient Rome, trade took place in the forum.[9] Rome had two forums; the Forum Romanum and Trajan's Forum. The latter was a vast expanse," +
                    " comprising multiple buildings with shops on four levels.[10] The Roman forum was arguably the earliest example of a permanent retail shop-front.[11] In antiquity, exchange involved direct selling via merchants or peddlers and bartering systems were commonplace.",
                    AuthorId = Uther2_Id,
                    IndustryId = Retail_Id,
                },
                new Report
                {
                    Id = Energy_Industry_Id,
                    Name = "Are electric utilities prepared for a low carbon future?",
                    Summary = "Linking emissions-related metrics to earnings for European electric utilities",
                    Description = "This report is the second in a series of quarterlyreports covering six high-emitting sectors (transport,electric utilities, materials, metal & mining, oil & gas,and consumer goods). In February, we publishedour first report in the series, covering " +
                            "the global automanufacturers and launching our new Super-LeagueTable (SLT) approach. The CDP Super-League Tableranks companies in an industry grouping on a numberof environmental metrics relevant to that industry, whichin aggregate could have a material impact on " +
                            "companyearnings and therefore impact investment decisions.In this report, we launch a Super-League Table forEuropean electric utilities. We rank those companiesthat responded to CDP’s Climate Change questionnaire,which account for c80%1 of electricity produced " +
                            "byEuropean electric utilities, based on a number of differentemissions-related metrics. When taken in aggregate, webelieve these metrics could have a material impact on acompany’s earnings in a European electric utility marketwhere the regulator seeks to cut" +
                            " greenhouse gas (GHG)emissions in the EU by 40% by 2030 (and 80% by 2050)from 1990 levels. To meet this target, more than 45%of European electricity generation would need to comefrom renewable energy sources (renewables) by 2030,up from 25% in 2013; and a switch " +
                            "from coal back togas generation would also be required. Additionally, thiswould require a functional carbon market, with a carbonprice significantly higher than today’s price2 under the EUEmissions Trading Scheme (ETS). ",
                    AuthorId = Mephisto_Id,
                    IndustryId = Electric_Utilities_Id,
                }
           );

            var yani = new User
            {
                Id = Yani_Id,
                Name = "Yani Yordanov",
                UserName = "qni_1999@mail.bg",
                NormalizedUserName = "QNI_1999@MAIL.BG",
                Email = "qni_1999@mail.bg",
                NormalizedEmail = "QNI_1999@MAIL.BG",
                Role = "Customer",
                IsApprovedByAdmin = true,
                PhoneNumber = "+359 000 000 000",
                SecurityStamp = Guid.NewGuid().ToString().ToUpper()
            };
            yani.PasswordHash = new PasswordHasher<User>().HashPassword(yani, "12345678");
            var pesho = new User
            {
                Id = Pesho_Id,
                Name = "Peter Stoyanov",
                UserName = "pesho_1999@mail.bg",
                NormalizedUserName = "PESHO_1999@MAIL.BG",
                Email = "pesho_1999@mail.bg",
                NormalizedEmail = "PESHO_1999@MAIL.BG",
                IsApprovedByAdmin = true,
                Role = "Author",
                PhoneNumber = "+359 000 000 000",
                SecurityStamp = Guid.NewGuid().ToString().ToUpper()
            };
            pesho.PasswordHash = new PasswordHasher<User>().HashPassword(pesho, "12345678");
            var baal = new User
            {
                Id = Baal_Id,
                Name = "Baal Stoyanov",
                Email = "Baal_Id_1999@mail.bg",
                UserName = "Baal_Id_1999@mail.bg",
                NormalizedUserName = "BAAL_ID_1999@MAIL.BG",
                NormalizedEmail = "BAAL_ID_1999@MAIL.BG",
                IsApprovedByAdmin = true,
                Role = "Author",
                PhoneNumber = "+359 000 000 000",
                SecurityStamp = Guid.NewGuid().ToString()
            };
            baal.PasswordHash = new PasswordHasher<User>().HashPassword(baal, "12345678");
            var mephisto = new User
            {
                Id = Mephisto_Id,
                Name = "Mephisto Stoyanov",
                Email = "Mephisto_1999@mail.bg",
                UserName = "Mephisto_1999@mail.bg",
                NormalizedUserName = "MEPHISTO_1999@MAIL.BG",
                NormalizedEmail = "MEPHISTO_1999@MAIL.BG",
                IsApprovedByAdmin = true,
                Role = "Author",
                PhoneNumber = "+359 000 000 000",
                SecurityStamp = Guid.NewGuid().ToString()
            };
            mephisto.PasswordHash = new PasswordHasher<User>().HashPassword(mephisto, "12345678");
            var radko = new User
            {
                Id = Radko_Id,
                Name = "Radko Stanev",
                Email = "radko@telerik.com",
                UserName = "radko@telerik.com",
                NormalizedUserName = "RADKO@TELERIK.COM",
                NormalizedEmail = "RADKO@TELERIK.COM",
                IsApprovedByAdmin = true,
                Role = "Customer",
                PhoneNumber = "+359 111 222 333",
                SecurityStamp = Guid.NewGuid().ToString()
            };
            radko.PasswordHash = new PasswordHasher<User>().HashPassword(radko, "12345678");

            var kiro = new User
            {
                Id = Kiro_Id,
                Name = "Kiril Stanoev",
                Email = "kiril@telerik.com",
                UserName = "kiril@telerik.com",
                NormalizedUserName = "KIRIL@TELERIK.COM",
                NormalizedEmail = "KIRIL@TELERIK.COM",
                IsApprovedByAdmin = true,
                Role = "Customer",
                PhoneNumber = "+359 333 222 111",
                SecurityStamp = Guid.NewGuid().ToString()
            };
            kiro.PasswordHash = new PasswordHasher<User>().HashPassword(kiro, "12345678");

            var edward = new User
            {
                Id = Edward_Id,
                Name = "Edward Nikolaev",
                Email = "edward@telerik.com",
                UserName = "edward@telerik.com",
                NormalizedUserName = "EDWARD@TELERIK.COM",
                NormalizedEmail = "EDWARD@TELERIK.COM",
                IsApprovedByAdmin = true,
                Role = "Customer",
                PhoneNumber = "+359 111 333 111",
                SecurityStamp = Guid.NewGuid().ToString()
            };
            edward.PasswordHash = new PasswordHasher<User>().HashPassword(edward, "12345678");

            var uther = new User
            {
                Id = Uther_Id,
                Name = "Pete Stoyanov",
                Email = "pesh_1999@mail.bg",
                UserName = "pesh_1999@mail.bg",
                NormalizedUserName = "PESH_1999@MAIL.BG",
                NormalizedEmail = "PESH_1999@MAIL.BG",
                LockoutEnd = DateTime.UtcNow.AddDays(1),
                IsApprovedByAdmin = true,
                Role = "Customer",
                PhoneNumber = "+359 000 000 000",
                SecurityStamp = Guid.NewGuid().ToString()
            };
            uther.PasswordHash = new PasswordHasher<User>().HashPassword(uther, "12345678");

            var uther2 = new User
            {
                Id = Uther2_Id,
                Name = "Uther Stoyanov",
                Email = "Uther2_Id@mail.bg",
                UserName = "Uther2_Id@mail.bg",
                NormalizedUserName = "UT_1999@MAIL.BG",
                NormalizedEmail = "UT_1999@MAIL.BG",
                IsDeleted = true,
                IsApprovedByAdmin = true,
                Role = "Customer",
                PhoneNumber = "+359 000 000 000",
                SecurityStamp = Guid.NewGuid().ToString()
            };
            uther2.PasswordHash = new PasswordHasher<User>().HashPassword(uther2, "12345678");

            var admin = new User
            {
                Id = Admin_Id,
                Name = "Admin Adminov",
                Email = "admin@insighthub.com",
                UserName = "admin@insighthub.com",
                NormalizedUserName = "ADMIN@INSIGHTHUB.COM",
                NormalizedEmail = "ADMIN@INSIGHTHUB.COM",
                Role = "Admin",
                IsApprovedByAdmin = true,
                PhoneNumber = "+359 000 000 000",
                SecurityStamp = Guid.NewGuid().ToString()
            };
            admin.PasswordHash = new PasswordHasher<User>().HashPassword(admin, "admin1");

            builder.Entity<User>().HasData(yani);
            builder.Entity<User>().HasData(pesho);
            builder.Entity<User>().HasData(baal);
            builder.Entity<User>().HasData(mephisto);
            builder.Entity<User>().HasData(uther);
            builder.Entity<User>().HasData(uther2);
            builder.Entity<User>().HasData(admin);
            builder.Entity<User>().HasData(radko);
            builder.Entity<User>().HasData(kiro);
            builder.Entity<User>().HasData(edward);

            // ADMIN
            builder.Entity<IdentityUserRole<int>>().HasData(
                        new IdentityUserRole<int>
                        {
                            RoleId = AdminRole_Id,
                            UserId = Admin_Id,
                        });

            // Customers & Authors
            builder.Entity<IdentityUserRole<int>>().HasData(
            new IdentityUserRole<int>
            {
                RoleId = AuthorRole_Id,
                UserId = Pesho_Id,
            });

            builder.Entity<IdentityUserRole<int>>().HasData(
            new IdentityUserRole<int>
            {
                RoleId = AdminRole_Id,
                UserId = Yani_Id,
            });

            builder.Entity<IdentityUserRole<int>>().HasData(
            new IdentityUserRole<int>
            {
                RoleId = AuthorRole_Id,
                UserId = Baal_Id,
            });

            builder.Entity<IdentityUserRole<int>>().HasData(
            new IdentityUserRole<int>
            {
                RoleId = Customer_Id,
                UserId = Mephisto_Id,
            });

            builder.Entity<IdentityUserRole<int>>().HasData(
            new IdentityUserRole<int>
            {
                RoleId = AuthorRole_Id,
                UserId = Uther_Id,
            });

            builder.Entity<IdentityUserRole<int>>().HasData(
            new IdentityUserRole<int>
            {
                RoleId = Customer_Id,
                UserId = Uther2_Id,
            });

            builder.Entity<IdentityUserRole<int>>().HasData(
            new IdentityUserRole<int>
            {
                RoleId = Customer_Id,
                UserId = Radko_Id,
            });

            builder.Entity<IdentityUserRole<int>>().HasData(
            new IdentityUserRole<int>
            {
                RoleId = Customer_Id,
                UserId = Kiro_Id,
            });

            builder.Entity<IdentityUserRole<int>>().HasData(
            new IdentityUserRole<int>
            {
                RoleId = Customer_Id,
                UserId = Edward_Id,
            });
        }
    }
}

