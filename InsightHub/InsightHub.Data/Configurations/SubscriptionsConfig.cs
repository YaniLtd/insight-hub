﻿using InsightHub.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace InsightHub.Data.Configurations
{
    public class SubscriptionsConfig : IEntityTypeConfiguration<Subscription>
    {
        public void Configure(EntityTypeBuilder<Subscription> builder)
        {
            builder.HasKey(x => new { x.UserId, x.IndustryId });

            builder.HasOne(x => x.User)
                .WithMany(x => x.Subscriptions)
                .HasForeignKey(x => x.UserId);

            builder.HasOne(x => x.Industry)
                .WithMany(x => x.Subscriptions)
                .HasForeignKey(x => x.IndustryId);
        }
    }
}
