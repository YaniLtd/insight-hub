﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;
using InsightHub.Data.Entities;

namespace InsightHub.Data.Configurations
{
    public class UserIndustriesConfig : IEntityTypeConfiguration<UserIndustries>
    {
        public void Configure(EntityTypeBuilder<UserIndustries> builder)
        {
            builder.HasKey(x => new { x.UserId, x.IndustryId });

            builder.HasOne(x => x.User)
                .WithMany(x => x.Industries)
                .HasForeignKey(x => x.UserId);

            builder.HasOne(x => x.Industry)
                .WithMany(x => x.Users)
                .HasForeignKey(x => x.IndustryId);
        }
    }
}

