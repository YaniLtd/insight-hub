﻿using InsightHub.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace InsightHub.Data.Configurations
{
    public class ReportTagsConfig : IEntityTypeConfiguration<ReportTags>
    {
        public void Configure(EntityTypeBuilder<ReportTags> builder)
        {
            builder.HasKey(x => new { x.ReportId, x.TagId });

            builder.HasOne(x => x.Report)
                .WithMany(x => x.ReportTags)
                .HasForeignKey(x => x.ReportId);

            builder.HasOne(x => x.Tag)
                .WithMany(x => x.Reports)
                .HasForeignKey(x => x.TagId);
        }
    }
}
