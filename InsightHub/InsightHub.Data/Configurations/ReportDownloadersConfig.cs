﻿using InsightHub.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace InsightHub.Data.Configurations
{
    public class ReportDownloadersConfig : IEntityTypeConfiguration<ReportDownloaders>
    {
        public void Configure(EntityTypeBuilder<ReportDownloaders> builder)
        {
            builder.HasKey(x => new { x.ReportId, x.DownloaderId });

            builder.HasOne(x => x.Report)
                .WithMany(x => x.ReportDownloaders)
                .HasForeignKey(x => x.ReportId)
                .OnDelete(DeleteBehavior.Restrict);

            builder.HasOne(x => x.Downloader)
                .WithMany(x => x.ReportDownloaders)
                .HasForeignKey(x => x.DownloaderId)
                .OnDelete(DeleteBehavior.Restrict);

        }
    }
}
