﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace InsightHub.Data.Entities
{
    public class User : IdentityUser<int>
    {
        //public new DateTimeOffset LockoutEnd { get; set; } = DateTime.UtcNow;
        public bool IsDeleted { get; set; } = false;
        public string Name { get; set; }
        public string Token { get; set; }
        public string Role { get; set; }
        public bool IsApprovedByAdmin { get; set; } = false;
        public IEnumerable<Subscription> Subscriptions { get; set; } = new HashSet<Subscription>();
        public IEnumerable<Ban> Bans { get; set; } = new HashSet<Ban>();
        public IEnumerable<Report> Reports { get; set; } = new HashSet<Report>();
        public IEnumerable<UserIndustries> Industries { get; set; } = new HashSet<UserIndustries>();
        public IEnumerable<ReportDownloaders> ReportDownloaders { get; set; } = new HashSet<ReportDownloaders>();
    }
}
