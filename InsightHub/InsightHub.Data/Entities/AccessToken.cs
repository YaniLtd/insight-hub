﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace InsightHub.Data.Entities
{
    public class AccessToken
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public Guid Value { get; set; }
        public DateTime ExpiresOn { get; set; }
        public bool HasExpired { get => ExpiresOn < DateTime.UtcNow; }
    }
}
