﻿using InsightHub.Data.Entities.Abstract;
using System;
using System.Collections.Generic;
using System.Text;

namespace InsightHub.Data.Entities
{
    public class Subscription : Entity
    {
        public int UserId { get; set; }
        public User User { get; set; }
        public int IndustryId { get; set; }
        public Industry Industry { get; set; }
    }
}
