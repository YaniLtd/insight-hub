﻿using System.ComponentModel.DataAnnotations.Schema;
using InsightHub.Data.Entities.Abstract;
using System.Collections.Generic;

namespace InsightHub.Data.Entities
{
    public class Industry : Entity
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string ImgUrl { get; set; }
        public string Name { get; set; }
        public IEnumerable<Report> Reports { get; set; }
        public IEnumerable<UserIndustries> Users { get; set; }
        public IEnumerable<Subscription> Subscriptions { get; set; }
    }
}

