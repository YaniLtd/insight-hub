﻿using System.ComponentModel.DataAnnotations.Schema;
using InsightHub.Data.Entities.Abstract;
using System.Collections.Generic;

namespace InsightHub.Data.Entities
{
    public class Tag : Entity
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string Name { get; set; }
        public ICollection<ReportTags> Reports { get; set; }
    }
}
