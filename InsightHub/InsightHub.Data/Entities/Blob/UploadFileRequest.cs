﻿namespace InsightHub.Data.Entities.Blob
{
    public class UploadFileRequest
    {
        public string FilePath { get; set; }
        public string FileName { get; set; }
    }
}
