﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InsightHub.Data.Entities.Contracts
{
    public interface IDeletable
    {
        public bool IsDeleted { get; set; }
        DateTime? DeletedOn { get; set; }
    }
}
