﻿namespace InsightHub.Web.Models
{
    /// <summary>
    /// A model class that is used to take/send User concerning data from/to the browser 
    /// </summary>
    public class UserViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public bool IsApprovedByAdmin { get; set; }
        public bool IsBanned { get; set; }
    }
}
