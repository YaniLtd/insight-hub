﻿namespace InsightHub.Web.Models
{
    /// <summary>
    /// A model class that is used to take/send Industry concerning data from/to the browser 
    /// </summary>
    public class IndustryViewModel
    {
        public int Id { get; set; }
        public string ImgUrl { get; set; }
        public string Name { get; set; }
    }
}
