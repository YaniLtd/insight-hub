﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace InsightHub.Web.Models
{
    /// <summary>
    /// A model class that is used to take/send Ban concerning data from/to the browser 
    /// </summary>
    public class BanViewModel
    {
        [DisplayName("Reason")]
        public string Name { get;set; }
        [Required(ErrorMessage = "Description is required!")]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.", MinimumLength = 6)]
        public string Description { get; set; }
        public int UserId { get; set; }
        [DataType(DataType.Date)]
        public DateTime ExpiresOn { get; set; }
        public bool HasExpired { get => (this.ExpiresOn < DateTime.UtcNow) ? true : false; } 
    }
}
