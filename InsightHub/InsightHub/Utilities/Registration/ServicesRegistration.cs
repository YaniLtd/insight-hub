﻿using Microsoft.Extensions.DependencyInjection;
using InsightHub.Services.Services.Main;
using InsightHub.Services.Services.Help;
using InsightHub.Services.Contracts;

namespace InsightHub.Web.Utilities.Registration
{
    public static class ServicesRegistration
    {
        public static IServiceCollection AddBusinessServices(this IServiceCollection services)
        {
            services.AddScoped<IReportService, ReportService>();
            services.AddScoped<ITagService, TagService>();
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IBanService, BanService>();
            services.AddScoped<IReportTagsService, ReportTagsService>();
            services.AddScoped<IFilterReportsService, FilterReportsService>();
            services.AddScoped<IIndustryService, IndustryService>();
            services.AddScoped<ISubscriptionService, SubscriptionService>();
            services.AddScoped<IRoleService, RoleService>();
            //services.AddScoped<IAuthenticateUserService, AuthenticateUserService>();

            return services;
        }
    }
}
