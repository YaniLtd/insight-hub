﻿using Microsoft.Extensions.DependencyInjection;
using InsightHub.Web.WebMappers.Contracts;
using InsightHub.Web.WebMappers;
using InsightHub.Services.DTOs;
using InsightHub.Web.Models;

namespace InsightHub.Web.Utilities.Registration
{
    public static class ViewModelMappersRegistration
    {
        public static IServiceCollection AddViewModelMappers(this IServiceCollection services)
        {
            services.AddSingleton<IViewModelMapper<ReportDTO, ReportViewModel>, ReportWebMapper>();
            services.AddSingleton<IViewModelMapper<IndustryDTO, IndustryViewModel>, IndustryWebMapper>();
            services.AddSingleton<IViewModelMapper<UserDTO, UserViewModel>, UserWebMapper>();
            services.AddSingleton<IViewModelMapper<BanDTO, BanViewModel>, BanWebMapper>();
            services.AddSingleton<IViewModelMapper<TagDTO, TagViewModel>, TagMapper>();
            return services;
        }
    }
}
