﻿using InsightHub.Web.WebMappers.Contracts;
using Microsoft.AspNetCore.Authorization;
using InsightHub.Services.Contracts;
using InsightHub.Services.DTOs;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using System.Security.Claims;
using InsightHub.Web.Models;
using NToastNotify;
using System;

namespace InsightHub.Web.Controllers.Customer
{
    [Authorize(Roles = "Customer")]
    public class CustomerReportController : Controller
    {
        private readonly IBlobService blobService;
        private readonly IReportService reportService;
        private readonly IToastNotification toastNotification;
        private readonly IViewModelMapper<ReportDTO, ReportViewModel> reportMapper;

        public CustomerReportController(IReportService reportService,
            IViewModelMapper<ReportDTO, ReportViewModel> reportMapper,
            IToastNotification toastNotification, IBlobService blobService)
        {
            this.blobService = blobService ?? throw new ArgumentNullException(nameof(blobService));
            this.reportMapper = reportMapper ?? throw new ArgumentNullException(nameof(reportMapper));
            this.reportService = reportService ?? throw new ArgumentNullException(nameof(reportService));
            this.toastNotification = toastNotification ?? throw new ArgumentNullException(nameof(toastNotification));
        }

        [HttpGet]
        public async Task<IActionResult> DownloadReport(int? id)
        {
            try
            {
                var downloaderId = int.Parse(User.FindFirstValue(ClaimTypes.NameIdentifier));
                var report = await this.reportService.DownloadReportAsync(downloaderId, id);
                var reportsVM = this.reportMapper.MapFrom(report);
                var data = await blobService.GetBlobAsync(report.Name + ".pdf");
                return File(data.Content, data.ContentType);
            }

            catch (Exception e)
            {
                this.toastNotification.AddErrorToastMessage(e.Message);
                return RedirectToAction("GetDownloadedReportsByUser");
            }
        }

        public async Task<IActionResult> GetDownloadedReportsByUser()
        {
            try
            {
                var user = int.Parse(User.FindFirstValue(ClaimTypes.NameIdentifier));
                var reports = await this.reportService.GetDownloadedReportsByUserAsync(user);
                var reportsVM = this.reportMapper.MapFrom(reports);
                return View(reportsVM);
            }

            catch (Exception e)
            {
                this.toastNotification.AddErrorToastMessage(e.Message);
                return View();
            }
        }
    }
}
