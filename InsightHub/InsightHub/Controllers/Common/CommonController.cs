﻿using InsightHub.Web.WebMappers.Contracts;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Authorization;
using InsightHub.Services.Contracts;
using System.Collections.Generic;
using InsightHub.Services.DTOs;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;
using System.Threading.Tasks;
using InsightHub.Web.Models;
using NToastNotify;
using System.Linq;
using System;

namespace InsightHub.Web.Controllers.Common
{
    public class CommonController : Controller
    {
        private readonly ITagService tagService;
        private readonly IReportService reportService;
        private readonly IIndustryService industryService;
        private readonly IToastNotification toastNotification;
        private readonly IViewModelMapper<TagDTO, TagViewModel> tagMapper;
        private readonly IViewModelMapper<ReportDTO, ReportViewModel> reportMapper;
        private readonly IViewModelMapper<IndustryDTO, IndustryViewModel> industryMapper;

        public CommonController(IReportService reportService,
            IViewModelMapper<ReportDTO, ReportViewModel> reportMapper,
            IViewModelMapper<IndustryDTO, IndustryViewModel> industryMapper,
            IToastNotification toastNotification, IIndustryService industryService,
            IViewModelMapper<TagDTO, TagViewModel> tagMapper, ITagService tagService)
        {
            this.toastNotification = toastNotification ?? throw new ArgumentNullException(nameof(toastNotification));
            this.industryService = industryService ?? throw new ArgumentNullException(nameof(industryService));
            this.industryMapper = industryMapper ?? throw new ArgumentNullException(nameof(industryMapper));
            this.reportService = reportService ?? throw new ArgumentNullException(nameof(reportService));
            this.reportMapper = reportMapper ?? throw new ArgumentNullException(nameof(reportMapper));
            this.tagService = tagService ?? throw new ArgumentNullException(nameof(tagService));
            this.tagMapper = tagMapper ?? throw new ArgumentNullException(nameof(tagMapper));
        }

        [HttpGet]
        [Authorize(Roles = "Admin, Author")]
        public async Task<IActionResult> CreateReport()

        {
            var tags = await tagService.GetAllTagsAsync();
            var tagsVM = this.tagMapper.MapFrom(tags);
            var selectList = new List<SelectListItem>();
            var tagsVMList = tagsVM.ToList();

            tagsVMList.ForEach(i => selectList.Add(new SelectListItem(i.Name, i.Id.ToString())));
            var mapToSelectList = new ReportViewModel() { Tags = selectList };

            ViewData["Id"] = new SelectList(await industryService.GetAllIndustries(), "Id", "Name");
            return View(mapToSelectList);
        }

        [HttpPost]
        [Authorize(Roles = "Author, Admin")]
        public async Task<IActionResult> CreateReport(ReportViewModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    this.toastNotification.AddErrorToastMessage("Model state is not valid.");
                    return RedirectToAction(nameof(CreateReport));
                }
                model.AuthorId = int.Parse(User.FindFirstValue(ClaimTypes.NameIdentifier));
                var modelToDTO = this.reportMapper.MapFrom(model);
                await this.reportService.CreateReportAsync(modelToDTO);

                this.toastNotification.AddSuccessToastMessage("Report successfully created. You will be notified " +
                    "by email from Insight Hub Team, when the report is approved");
                return RedirectToAction(nameof(CreateReport));
            }
            catch (Exception e)
            {
                this.toastNotification.AddErrorToastMessage(e.Message);
                return RedirectToAction(nameof(CreateReport));
            }
        }

        [HttpGet]
        [Authorize(Roles = "Author, Admin")]
        public async Task<IActionResult> UpdateReport(int? id)

        {
            var report = await reportService.GetReportAsync(id.Value);
            var tags = await tagService.GetAllTagsAsync();
            var tagsVM = this.tagMapper.MapFrom(tags);
            var selectList = new List<SelectListItem>();
            var tagsVMList = tagsVM.ToList();

            tagsVMList.ForEach(i => selectList.Add(new SelectListItem(i.Name, i.Id.ToString())));
            var mapToSelectList = new ReportViewModel() { Tags = selectList };

            ViewData["Id"] = new SelectList(await industryService.GetAllIndustries(), "Id", "Name");
            var reportVM = reportMapper.MapFrom(report);
            reportVM.Tags = selectList;
            ViewData["Model"] = reportVM;
            return View(reportVM);
        }

        [HttpPost]
        [Authorize(Roles = "Admin, Author")]
        public async Task<IActionResult> UpdateReport(int? id, ReportViewModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    ModelState.AddModelError(string.Empty, "Model state is not valid.");
                }

                var modelToDTO = this.reportMapper.MapFrom(model);
                await this.reportService.UpdateReportAsync(modelToDTO);
                this.toastNotification.AddSuccessToastMessage("Report successfully updated");
                return RedirectToAction(nameof(UpdateReport));
            }

            catch (Exception e)
            {
                this.toastNotification.AddErrorToastMessage(e.Message);
                return RedirectToAction(nameof(UpdateReport));
            }
        }

        [HttpGet]
        [Authorize(Roles = "Author, Admin")]
        public IActionResult CreateTag()

        {
            return View();
        }

        [HttpPost]
        [Authorize(Roles = "Author, Admin")]
        public async Task<IActionResult> CreateTag(TagViewModel model)
        {
            if (ModelState.IsValid)
            {
                var modelToDTO = this.tagMapper.MapFrom(model);
                await this.tagService.CreateTagAsync(modelToDTO);
                this.toastNotification.AddSuccessToastMessage("Tag successfully created");

                return RedirectToAction(nameof(CreateReport));
            }

            this.toastNotification.AddWarningToastMessage("Incorrect input, please try again");
            ModelState.AddModelError(string.Empty, "Model state is not valid.");
            return RedirectToAction("CreateTag");
        }

        [HttpGet]
        [Authorize(Roles = "Author, Admin")]
        public async Task<IActionResult> UpdateTagAsync(int? id)
        {
            try
            {
                var tag = await this.tagService.GetTagAsync(id.Value);
                var modelToDTO = this.tagMapper.MapFrom(tag);
                return View(modelToDTO);
            }
            catch (Exception)
            {
                this.toastNotification.AddWarningToastMessage("Oops! Something went wrong. Please try again");
                return View();
            }
        }

        [HttpPost]
        [Authorize(Roles = "Author, Admin")]
        public async Task<IActionResult> UpdateTag(TagViewModel model)
        {
            if (ModelState.IsValid)
            {
                var modelToDTO = this.tagMapper.MapFrom(model);
                await this.tagService.UpdateTagAsync(modelToDTO);
                this.toastNotification.AddSuccessToastMessage("Tag successfully updated");
                return RedirectToAction("GetAllTags", "AdminTag"); 
            }

            this.toastNotification.AddWarningToastMessage("Incorrect input, please try again");
            ModelState.AddModelError(string.Empty, "Model state is not valid.");
            return RedirectToAction("UpdateTag");
        }
    }
}
