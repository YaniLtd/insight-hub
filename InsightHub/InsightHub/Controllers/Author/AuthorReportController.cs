﻿using InsightHub.Web.WebMappers.Contracts;
using Microsoft.AspNetCore.Authorization;
using InsightHub.Services.Contracts;
using InsightHub.Services.DTOs;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;
using System.Threading.Tasks;
using InsightHub.Web.Models;
using NToastNotify;
using System;

namespace InsightHub.Web.Controllers.Author
{
    /// <summary>
    /// This class contains methods for working with reports
    /// invokable by the author
    /// </summary>
    [Authorize(Roles = "Author")]
    public class AuthorReportController : Controller
    {
        private readonly IBlobService blobService;
        private readonly IReportService reportService;
        private readonly IToastNotification toastNotification;
        private readonly IViewModelMapper<ReportDTO, ReportViewModel> reportMapper;

        public AuthorReportController(IReportService reportService,
            IViewModelMapper<ReportDTO, ReportViewModel> reportMapper,
            IToastNotification toastNotification, IBlobService blobService)
        {
            this.toastNotification = toastNotification ?? throw new ArgumentNullException(nameof(toastNotification));
            this.reportService = reportService ?? throw new ArgumentNullException(nameof(reportService));
            this.reportMapper = reportMapper ?? throw new ArgumentNullException(nameof(reportMapper));
            this.blobService = blobService ?? throw new ArgumentNullException(nameof(blobService));
        }

        [HttpGet]
        public async Task<IActionResult> GetReportsOfUser()
        {
            try
            {
                var authorId = int.Parse(User.FindFirstValue(ClaimTypes.NameIdentifier));
                var reports = await this.reportService.GetReportsOfUserAsync(authorId);
                var reportsVM = this.reportMapper.MapFrom(reports);
                return View(reportsVM);
            }

            catch (Exception e)
            {
                this.toastNotification.AddErrorToastMessage(e.Message);
                return RedirectToAction("Index", "Report");
            }
        }

        [HttpGet]
        public async Task<IActionResult> DownloadReport(int? id)
        {
            try
            {
                var downloaderId = int.Parse(User.FindFirstValue(ClaimTypes.NameIdentifier));
                var report = await this.reportService.DownloadReportAsync(downloaderId, id);
                var reportsVM = this.reportMapper.MapFrom(report);
                var data = await blobService.GetBlobAsync(report.Name + ".pdf");
                return File(data.Content, data.ContentType);
            }

            catch (Exception e)
            {
                this.toastNotification.AddErrorToastMessage(e.Message);
                return RedirectToAction("GetReportsOfUser");
            }
        }

        [HttpGet]
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound("Id cannot be null or empty");
            }

            var report = await this.reportService.GetReportAsync(id.Value);

            if (report == null)
            {
                return NotFound("Report cannot be null or empty");
            }

            return View(reportMapper.MapFrom(report));
        }

        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound("Id cannot be null or empty");
            }

            var report = await this.reportService.DeleteReportAsync(id);
            this.toastNotification.AddSuccessToastMessage("Report successfully deleted");
            return RedirectToAction("GetReportsOfUser");
        }
    }
}
