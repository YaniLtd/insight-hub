﻿using InsightHub.Services.Contracts;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Linq.Dynamic.Core;
using System.Security.Claims;
using System.Threading.Tasks;
using NToastNotify;
using System.Linq;
using System;

namespace InsightHub.Web.Controllers.Anonymous
{
    /// <summary>
    /// Generates tables which have sorting, searching and paging features
    /// </summary>
    public class SearchController : Controller
    {
        private readonly ISubscriptionService subscriptionService;
        private readonly IToastNotification toastNotification;
        private readonly IIndustryService industryService;
        private readonly IReportService reportService;
        private readonly IUserService userService;
        private readonly ITagService tagService;

        public SearchController(IReportService reportService, IIndustryService industryService,
            ITagService tagService, IUserService userService, IToastNotification toastNotification,
            ISubscriptionService subscriptionService)
        {
            this.subscriptionService = subscriptionService;
            this.toastNotification = toastNotification;
            this.industryService = industryService;
            this.reportService = reportService;
            this.userService = userService;
            this.tagService = tagService;
        }

        public async Task<IActionResult> GetDownloadedReportsByUser()
        {
            var loggedUserId = int.Parse(User.FindFirstValue(ClaimTypes.NameIdentifier));
            var reportsOfUser = await this.reportService.GetDownloadedReportsByUserAsync(loggedUserId);
            var reports = reportsOfUser.AsQueryable();
            return DisplayTable(reports);
        }
        public async Task<IActionResult> GetReportsOfUser()
        {
            var loggedUserId = int.Parse(User.FindFirstValue(ClaimTypes.NameIdentifier));
            var reportsOfUser = await this.reportService.GetReportsOfUserAsync(loggedUserId);
            var reports = reportsOfUser.AsQueryable();
            return DisplayTable(reports);
        }

        public async Task<IActionResult> LoadTags()
        {
            var getAllTags = await tagService.GetAllTagsAsync();
            var tags = getAllTags.AsQueryable();
            return DisplayTable(tags);
        }

        public async Task<IActionResult> LoadUserNonSubscriptions()
        {
            var loggedUserId = int.Parse(User.FindFirstValue(ClaimTypes.NameIdentifier));
            var nonSubscriptions = (await this.subscriptionService.GetUserNonSubscriptionsAsync(loggedUserId)).AsQueryable();
            return DisplayTable(nonSubscriptions);
        }

        public async Task<IActionResult> LoadSubscriptionsOfUser()
        {
            var loggedUserId = int.Parse(User.FindFirstValue(ClaimTypes.NameIdentifier));
            var subscriptions = (await this.subscriptionService.GetSubscriptionsOfUserAsync(loggedUserId)).AsQueryable();

            if (subscriptions.Count() == 0)
            {
                this.toastNotification.AddInfoToastMessage("You don't have an active subscription");
            }

            return DisplayTable(subscriptions);
        }

        public async Task<IActionResult> LoadIndustries()
        {
            var getAllIndustries = await industryService.GetAllIndustries();
            var industries = getAllIndustries.AsQueryable();
            return DisplayTable(industries);
        }

        public async Task<IActionResult> LoadReportsForApproval()
        {
            var reportsForApproval = await reportService.GetPendingReportsAsync();
            var reports = reportsForApproval.AsQueryable();

            if (reports.Count() == 0)
            {
                this.toastNotification.AddInfoToastMessage("No reports for approval");
            }

            return DisplayTable(reports);
        }

        public async Task<IActionResult> LoadUsersForApproval()
        {
            var usersForApproval = await userService.GetPendingUsersAsync();
            var users = usersForApproval.AsQueryable();

            if (users.Count() == 0)
            {
                this.toastNotification.AddInfoToastMessage("No users for approval");
            }

            return DisplayTable(users);
        }

        public async Task<IActionResult> LoadApprovedUsers()
        {
            var approvedUsers = await userService.GetAprovedUsersAsync();
            var users = approvedUsers.AsQueryable();
            return DisplayTable(users);
        }

        public async Task<IActionResult> LoadReports()
        {
            var reports = await reportService.GetAprovedReportsAsync();
            var reportDTOs = reports.AsQueryable();
            return DisplayTable(reportDTOs);
        }

        private IActionResult DisplayTable<T>(IQueryable<T> entities)
        {
            try
            {
                var draw = HttpContext.Request.Form["draw"].FirstOrDefault();

                // Skiping number of Rows count  
                var start = Request.Form["start"].FirstOrDefault();
                // Paging Length 10,20  
                var length = Request.Form["length"].FirstOrDefault();
                // Sort Column Name  
                var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
                // Sort Column Direction ( asc ,desc)  
                var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();
                // Search Value from (Search box)  
                var searchValue = Request.Form["search[value]"].FirstOrDefault();

                //Paging Size (10,20,50,100)  
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int recordsTotal = 0;

                // Getting all Customer data  
                var customerData = (from tempcustomer in entities
                                    select tempcustomer);

                // TODO: Sorting - use switch case to get property from SortColumn
                if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDirection)))
                {
                    customerData = customerData.OrderBy(sortColumn + " " + sortColumnDirection);
                }
                //Search  
                if (!string.IsNullOrEmpty(searchValue))
                {
                    customerData = customerData.Where(m => m.ToString().ToLower().Contains(searchValue.ToLower()));
                }
                //total number of rows count   
                recordsTotal = customerData.Count();
                //Paging   
                var data = customerData.Skip(skip).Take(pageSize).ToList();
                //Returning Json Data  
                return Json(new { draw, recordsFiltered = recordsTotal, recordsTotal, data });

            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}


