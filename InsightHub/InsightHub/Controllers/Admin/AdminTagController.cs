﻿using InsightHub.Web.WebMappers.Contracts;
using InsightHub.Services.Contracts;
using InsightHub.Services.DTOs;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using InsightHub.Web.Models;
using NToastNotify;
using System;
using Microsoft.AspNetCore.Authorization;

namespace InsightHub.Web.Controllers.Admin
{
    /// <summary>
    /// This class contains methods for working with tags
    /// that only the admin can invoke
    /// </summary>
    [Authorize(Roles = "Admin")]
    public class AdminTagController : Controller
    {
        private readonly ITagService tagService;
        private readonly IToastNotification toastNotification;
        private readonly IViewModelMapper<TagDTO, TagViewModel> tagMapper;

        public AdminTagController(ITagService tagService,
            IToastNotification toastNotification, IViewModelMapper<TagDTO, TagViewModel> tagMapper)
        {
            this.tagService = tagService ?? throw new ArgumentNullException(nameof(tagService));
            this.toastNotification = toastNotification ?? throw new ArgumentNullException(nameof(toastNotification));
            this.tagMapper = tagMapper ?? throw new ArgumentNullException(nameof(tagMapper));
        }

        [HttpGet]
        public async Task<IActionResult> GetAllTags()
        {
            try
            {
                var tags = await this.tagService.GetAllTagsAsync();
                var tagVM = this.tagMapper.MapFrom(tags);
                return View(tagVM);
            }
            catch (Exception)
            {
                this.toastNotification.AddWarningToastMessage("Oops! Something went wrong. Please try again");
                return View();
            }
        }

        [HttpGet]
        public async Task<IActionResult> GetTag(int id)
        {
            try
            {
                var tag = await this.tagService.GetTagAsync(id);
                return View(tag);
            }
            catch (Exception)
            {
                this.toastNotification.AddWarningToastMessage("Oops! Something went wrong. Please try again");
                return View();
            }
        }

        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound("Id cannot be null or empty");
            }

            var report = await this.tagService.DeleteTagAsync(id);
            this.toastNotification.AddSuccessToastMessage("Tag successfully deleted");
            return RedirectToAction("GetAllTags");
        }
    }
}
