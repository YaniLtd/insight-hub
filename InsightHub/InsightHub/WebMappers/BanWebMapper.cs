﻿using System.Collections.Generic;
using InsightHub.Services.DTOs;
using InsightHub.Web.Models;
using System.Linq;
using System;

namespace InsightHub.Web.WebMappers.Contracts
{
    public class BanWebMapper : IViewModelMapper<BanDTO, BanViewModel>, IBanWebMapper
    {
        public BanViewModel MapFrom(BanDTO entity)
        {
            if (entity == null)
            {
                throw new ArgumentException("DTO not found");
            }

            return new BanViewModel
            {
                Name = entity.Name,
                Description = entity.Description,
                UserId = entity.UserId                       
            };
        }

        public IEnumerable<BanViewModel> MapFrom(IEnumerable<BanDTO> entities)
        {
            return entities.Select(x => MapFrom(x));
        }

        public BanDTO MapFrom(BanViewModel entityVM)
        {
            if (entityVM == null)
            {
                throw new ArgumentException("View Model not found");
            }

            return new BanDTO
            {
                Name = entityVM.Name,
                Description = entityVM.Description,
                UserId = entityVM.UserId,
                ExpiresOn = entityVM.ExpiresOn,
            };
        }

        public IEnumerable<BanDTO> MapFrom(ICollection<BanViewModel> entitiesVM)
        {
            return entitiesVM.Select(x => MapFrom(x));
        }
    }
}
