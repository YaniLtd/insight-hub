﻿using System.Collections.Generic;
using InsightHub.Services.DTOs;
using InsightHub.Web.Models;
using System.Linq;
using System;

namespace InsightHub.Web.WebMappers.Contracts
{
    public class ReportWebMapper : IViewModelMapper<ReportDTO, ReportViewModel>, IReportWebMapper
    {
        public ReportViewModel MapFrom(ReportDTO entity)
        {
            if (entity == null)
            {
                throw new ArgumentException("DTO not found");
            }

            return new ReportViewModel
            {
                Id = entity.Id,
                Name = entity.Name,
                Description = entity.Description,
                Summary = entity.Summary,
                AuthorId = entity.AuthorId,
                AuthorName = entity.AuthorName,
                IndustryId = entity.IndustryId,
                IndustryImg = entity.IndustryImg,
                IndustryName = entity.IndustryName,
                TagNames = entity.TagNames,            
                NamesOfDownloaders = entity.NamesOfDownloaders,
                BinaryContent = entity.BinaryContent,
                DownloadsCnt = entity.DownloadsCnt,
                UploadedOn = entity.UploadedOn,
                IsPending = entity.IsPending,
                IsFeatured = entity.IsFeatured
            };
        }

        public IEnumerable<ReportViewModel> MapFrom(IEnumerable<ReportDTO> entities)
        {
            return entities.Select(x => MapFrom(x));
        }

        public ReportDTO MapFrom(ReportViewModel entityVM)
        {
            if (entityVM == null)
            {
                throw new ArgumentException("View Model not found");
            }

            return new ReportDTO
            {
                Id = entityVM.Id,
                Name = entityVM.Name,
                Description = entityVM.Description,
                Summary = entityVM.Summary,
                AuthorName = entityVM.AuthorName,
                AuthorId = entityVM.AuthorId,
                IndustryName = entityVM.IndustryName,
                IndustryImg = entityVM.IndustryImg,
                IndustryId = entityVM.IndustryId,
                TagNames = entityVM.TagNames,
                TagsIds = entityVM.SelectedTags,
                NamesOfDownloaders = entityVM.NamesOfDownloaders,
                BinaryContent = entityVM.BinaryContent,
                DownloadsCnt = entityVM.DownloadsCnt,
                UploadedOn = entityVM.UploadedOn,
                IsPending = entityVM.IsPending,
                IsFeatured = entityVM.IsFeatured
            };
        }

        public IEnumerable<ReportDTO> MapFrom(ICollection<ReportViewModel> entitiesVM)
        {
            return entitiesVM.Select(x => MapFrom(x));
        }
    }
}
