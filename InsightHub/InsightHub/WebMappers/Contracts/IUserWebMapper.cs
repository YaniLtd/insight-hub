﻿using InsightHub.Services.DTOs;
using InsightHub.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InsightHub.Web.WebMappers.Contracts
{
    public interface IUserWebMapper
    {
        IEnumerable<UserViewModel> MapFrom(IEnumerable<UserDTO> entities);
        UserViewModel MapFrom(UserDTO entity);
        public UserDTO MapFrom(UserViewModel entityVM);
        public IEnumerable<UserDTO> MapFrom(ICollection<UserViewModel> entitiesVM);
    }
}
