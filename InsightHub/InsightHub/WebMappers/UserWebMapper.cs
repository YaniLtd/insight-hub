﻿using InsightHub.Web.WebMappers.Contracts;
using System.Collections.Generic;
using InsightHub.Services.DTOs;
using InsightHub.Web.Models;
using System.Linq;
using System;

namespace InsightHub.Web.WebMappers
{
    public class UserWebMapper : IViewModelMapper<UserDTO, UserViewModel>, IUserWebMapper
    {
        public UserViewModel MapFrom(UserDTO entity)
        {
            if (entity == null)
            {
                throw new ArgumentException("DTO not found");
            }

            return new UserViewModel
            {
                Id = entity.Id,
                IsBanned = entity.IsBanned,
                IsApprovedByAdmin = entity.IsApprovedByAdmin,
                Name = entity.Name,
                Email = entity.Email
            };
        }

        public IEnumerable<UserViewModel> MapFrom(IEnumerable<UserDTO> entities)
        {
            return entities.Select(x => MapFrom(x));
        }

        public UserDTO MapFrom(UserViewModel entityVM)
        {
            if (entityVM == null)
            {
                throw new ArgumentException("View Model not found");
            }

            return new UserDTO
            {
                Id = entityVM.Id,
                IsApprovedByAdmin = entityVM.IsApprovedByAdmin,
                Name = entityVM.Name,
                IsBanned = entityVM.IsBanned,
                Email = entityVM.Email
            };
        }

        public IEnumerable<UserDTO> MapFrom(ICollection<UserViewModel> entitiesVM)
        {
            return entitiesVM.Select(x => MapFrom(x));
        }
    }
}
