﻿using InsightHub.Services.Contracts;
using InsightHub.Services.DTOs;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using System;

namespace InsightHub.Web.Rest_Api
{
    [Route("api/[controller]s")]
    [ApiController]
    public class TagController : ControllerBase
    {
        private readonly ITagService tagService;

        public TagController(ITagService tagService)
        {
            this.tagService = tagService ?? throw new ArgumentNullException(nameof(tagService));
        }

        [HttpGet("")]
        public async Task<IActionResult> GetAllTags()
        {
            var tags = await this.tagService.GetAllTagsAsync();

            return Ok(tags);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetTag(int id)
        {
            try
            {
                var tag = await this.tagService.GetTagAsync(id);
                return Ok(tag);
            }

            catch (Exception)
            {
                return NotFound();
            }
        }

        [HttpPost("")]
        public async Task<IActionResult> CreateTag([FromBody] TagDTO tagDTO)
        {
            try
            {
                tagDTO = await this.tagService.CreateTagAsync(tagDTO);

                return Ok(tagDTO);
            }

            catch (Exception)
            {
                return BadRequest();
            }
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateTag(int? id, [FromBody] TagDTO tagDTO)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest();
                }

                await this.tagService.UpdateTagAsync(tagDTO);

                return Ok();
            }

            catch (Exception)
            {
                return BadRequest();
            }
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteTag(int id)
        {
            try
            {
                var tagToDelete = await this.tagService.DeleteTagAsync(id);
                return Ok(tagToDelete);
            }

            catch (Exception)
            {
                return BadRequest();
            }
        }
    }
}