﻿using Microsoft.AspNetCore.Authorization;
using InsightHub.Services.Contracts;
using Microsoft.AspNetCore.Mvc;
using InsightHub.Services.DTOs;
using System.Threading.Tasks;
using System;
using InsightHub.WebApi.Models;
using InsightHub.WebApi.Utilities;
using System.Web;

namespace InsightHub.Web.Rest_Api
{
    [Route("api/[controller]s")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUserService userService;
        private readonly IRoleService roleService;
        private readonly IAuthorizeService authorize;
        private readonly IAuthenticateUserService authenticateUserService;
        public UserController(IUserService userService, IRoleService roleService,
            IAuthenticateUserService authenticateUserService, IAuthorizeService authorize)
        {
            this.userService = userService ?? throw new ArgumentNullException(nameof(userService));
            this.roleService = roleService ?? throw new ArgumentNullException(nameof(roleService));
            this.authorize = authorize ?? throw new ArgumentNullException(nameof(authorize));
            this.authenticateUserService = authenticateUserService ??
                throw new ArgumentNullException(nameof(authenticateUserService));
        }

        [HttpPost("authenticate")]
        public async Task<IActionResult> Authenticate([FromBody]AuthenticateModel model)
        {
            try
            {
                var user = await authenticateUserService.AuthenticateAsync(model.Username, model.Password);

                if (user == null)
                    return BadRequest(new { message = "Username or password is incorrect" });
                return Ok(user);
            }
            catch (Exception e)
            {
                return Ok(e.Message);
            }

        }
        [HttpGet("")]
        public async Task<IActionResult> GetAllValidUsers([FromQuery] string token)
        {
            try
            {
                var requestingUser = await this.userService.GetUserByTokenAsync(token);
                authorize.ByRole(requestingUser, "Admin");

                var resultUsers = await this.userService.GetAllValidUsersAsync();

                return Ok(resultUsers);
            }

            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
        [HttpGet("{id}")]
        public async Task<IActionResult> GetUser(int id, [FromQuery] string token)
        {
            try
            {
                var requestingUser = await this.userService.GetUserByTokenAsync(token);
                authorize.ByRole(requestingUser, "Admin");

                var resultUser = await this.userService.GetUserAsync(id);

                return Ok(resultUser);
            }
            catch (HttpException e)
            {
                return StatusCode(e.StatusCode, e.StatusDescription);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
        [HttpGet("approved")]
        public async Task<IActionResult> GetAprovedUsers([FromQuery] string token)
        {
            try
            {
                var requestingUser = await this.userService.GetUserByTokenAsync(token);
                authorize.ByRole(requestingUser, "Admin");

                var resultUsers = await this.userService.GetAprovedUsersAsync();

                return Ok(resultUsers);
            }

            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
        [HttpGet("pending")]
        public async Task<IActionResult> GetPendingUsers([FromQuery] string token)
        {
            try
            {
                var requestingUser = await this.userService.GetUserByTokenAsync(token);
                authorize.ByRole(requestingUser, "Admin");

                var resultUsers = await this.userService.GetPendingUsersAsync();

                return Ok(resultUsers);
            }

            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpPut("approve/{id}")]
        public async Task<IActionResult> AproveUser(int id, [FromQuery] string token)
        {
            try
            {
                var requestingUser = await this.userService.GetUserByTokenAsync(token);
                authorize.ByRole(requestingUser, "Admin");
                var resultUser = await this.userService.AproveUserAsync(id);

                return Ok(resultUser);
            }
            catch (HttpException e)
            {
                return StatusCode(e.StatusCode, e.StatusDescription);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteUser(int id, [FromQuery] string token)
        {
            try
            {
                var requestingUser = await this.userService.GetUserByTokenAsync(token);
                authorize.ByRole(requestingUser, "Admin");
                await this.userService.DeleteUserAsync(id);

                return Ok("User account deleted successfully");
            }
            catch (HttpException e)
            {
                return StatusCode(e.StatusCode, e.StatusDescription);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
    }
}
