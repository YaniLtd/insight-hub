﻿using Microsoft.Extensions.DependencyInjection;
using InsightHub.Services.Mappers.Contracts;
using InsightHub.Services.Mappers;
using InsightHub.Data.Entities;
using InsightHub.Services.DTOs;

namespace InsightHub.Web.Utilities.Registration
{
    public static class DtoMapperRegistration
    {
        public static IServiceCollection AddDtoMappers(this IServiceCollection services)
        {
            services.AddSingleton<IDTOMapper<Tag, TagDTO>, TagMapper>();
            services.AddSingleton<IDTOMapper<Report, ReportDTO>, ReportMapper>();
            services.AddSingleton<IDTOMapper<User, UserDTO>, UserMapper>();
            services.AddSingleton<IDTOMapper<Ban, BanDTO>, BanMapper>();
            services.AddSingleton<IDTOMapper<Industry, IndustryDTO>, IndustryMapper>();

            return services;
        }
    }
}
