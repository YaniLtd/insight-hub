﻿using InsightHub.Services.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace InsightHub.WebApi.Utilities
{
    public class AuthorizeService : IAuthorizeService
    {
        public void ByRole(UserDTO userDTO, string roles)
        {
            if (!roles.Contains(userDTO.Role))
            {
                throw new HttpException(401, "Access denied");
            }
        }
    }
}
