﻿using InsightHub.Data.DataAccessContext;
using InsightHub.Data.Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using InsightHub.Services.Services.Main;
using InsightHub.Services.Mappers.Contracts;
using InsightHub.Services.DTOs;
using System.Linq;
using InsightHub.Services.Services.Help;

namespace InsightHub.Test.ReportService
{
    [TestClass]
    public class FilterReportsShould
    {
        private class FakeMap : IDTOMapper<Report, ReportDTO>
        {
            public ReportDTO MapFrom(Report entity)
            {
                return new ReportDTO
                {
                    Id = entity.Id,
                    Name = entity.Name,
                    Description = entity.Description,
                    AuthorId = entity.AuthorId,
                    IndustryId = entity.IndustryId,
                    IndustryName = entity.Industry.Name,
                    TagNames = entity.ReportTags.Select(x => x.Tag.Name),
                    IsPending = entity.IsPending
                };
            }

            public IEnumerable<ReportDTO> MapFrom(IEnumerable<Report> entities)
            {
                return entities.Select(x => MapFrom(x));
            }
        }
        [TestMethod]
        public async Task FilterReportsShould_Succeed_ByName()
        {

            var options = InsightHubUtils.GetOptions();
            
            Report[] expected = new Report[]
                {
                new Report
                {
                    Id = InsightHubUtils.WC3IsBeingSavedFromPad_Id,
                    Name = "WC3 is being saved from Pad!",
                    Description = "WC3 is being saved from Pad!",
                    AuthorId = InsightHubUtils.Yani_Id,
                    IndustryId = InsightHubUtils.Gaming_Id,
                },
                new Report
                {
                    Id = InsightHubUtils.HowGoodIsAO2DE_Id,
                    Name = "WC3 is being saved from Pad!",
                    Description = "WC3 is being saved from Pad!",
                    AuthorId = InsightHubUtils.Yani_Id,
                    IndustryId = InsightHubUtils.Gaming_Id,
                },
            };
            using (InsightHubContext context = new InsightHubContext(options))
            {
                InsightHubUtils.Seed(context);
            }
            using (InsightHubContext context = new InsightHubContext(options))
            {
                var sut = new FilterReportsService(new Services.Services.Main.ReportService(context, new FakeMap(),
                    InsightHubUtils.blobService, InsightHubUtils.mailer, 
                    new ReportTagsService(context), null));
                var result = (await sut.FilterReportsAsync("WC3 is being saved from Pad!", null, null)).ToArray();

                Assert.AreEqual(expected.Length, result.Length);
                for (int i = 0; i < result.Length; i++)
                {
                    Assert.AreEqual(expected[i].Id, result[i].Id);
                    Assert.AreEqual(expected[i].Name, result[i].Name);
                    Assert.AreEqual(expected[i].Description, result[i].Description);
                    Assert.AreEqual(expected[i].AuthorId, result[i].AuthorId);
                    Assert.AreEqual(expected[i].IndustryId, result[i].IndustryId);
                }
            }
        }
        
        [TestMethod]
        public async Task FilterReportsShould_Succeed_ByIndustryName()
        {

            var options = InsightHubUtils.GetOptions();
            Report[] expected = new Report[]
                {
                new Report
                {
                    Id = InsightHubUtils.WhatIsNewInJava8_Id,
                    Name = "What is new in Java 8?",
                    Description = "What is new in Java 8?",
                    AuthorId = InsightHubUtils.Pesho_Id,
                    IndustryId = InsightHubUtils.Gaming_Id,
                },
                new Report
                {
                    Id = InsightHubUtils.WC3IsBeingSavedFromPad_Id,
                    Name = "WC3 is being saved from Pad!",
                    Description = "WC3 is being saved from Pad!",
                    AuthorId = InsightHubUtils.Yani_Id,
                    IndustryId = InsightHubUtils.Gaming_Id,
                },
                new Report
                {
                    Id = InsightHubUtils.HowGoodIsAO2DE_Id,
                    Name = "WC3 is being saved from Pad!",
                    Description = "WC3 is being saved from Pad!",
                    AuthorId = InsightHubUtils.Yani_Id,
                    IndustryId = InsightHubUtils.Gaming_Id,
                    IsPending = true
                },
            };
            using (InsightHubContext context = new InsightHubContext(options))
            {
                InsightHubUtils.Seed(context);
            }
            using (InsightHubContext context = new InsightHubContext(options))
            {
                var sut = new FilterReportsService(new Services.Services.Main.ReportService(context, new FakeMap(),
                    InsightHubUtils.blobService, InsightHubUtils.mailer,
                    new ReportTagsService(context), null));
                var result = (await sut.FilterReportsAsync(null, null, "Gaming")).ToArray();

                Assert.AreEqual(expected.Length, result.Length);
                for (int i = 0; i < result.Length; i++)
                {
                    Assert.AreEqual(expected[i].Id, result[i].Id);
                    Assert.AreEqual(expected[i].Name, result[i].Name);
                    Assert.AreEqual(expected[i].Description, result[i].Description);
                    Assert.AreEqual(expected[i].AuthorId, result[i].AuthorId);
                    Assert.AreEqual(expected[i].IndustryId, result[i].IndustryId);
                }
            }
        }
        [TestMethod]
        public async Task FilterReportsShould_Succeed_ByTags()
        {

            var options = InsightHubUtils.GetOptions();
            var tags = new Tag[]
            {
                 new Tag
                 {
                     Id = InsightHubUtils.B2W_Id,
                     Name = "B2W"
                 },
                 new Tag
                 {
                     Id = InsightHubUtils.D2_Id,
                     Name = "D2"
                 },
            };

            Report[] expected = new Report[]
                {
                new Report
                {
                    Id = InsightHubUtils.HowGoodIsAO2DE_Id,
                    Name = "WC3 is being saved from Pad!",
                    Description = "WC3 is being saved from Pad!",
                    AuthorId = InsightHubUtils.Yani_Id,
                    IndustryId = InsightHubUtils.Gaming_Id,
                    IsPending = true
                },
            };
            using (InsightHubContext context = new InsightHubContext(options))
            {
                InsightHubUtils.Seed(context);
            }
            using (InsightHubContext context = new InsightHubContext(options))
            {
                var sut = new FilterReportsService(new Services.Services.Main.ReportService(context, new FakeMap(),
                    InsightHubUtils.blobService, InsightHubUtils.mailer, 
                    new ReportTagsService(context), null));
                var result = (await sut.FilterReportsAsync(null, tags.Select(x => x.Name).ToArray(), null)).ToArray();

                Assert.AreEqual(expected.Length, result.Length);
                for (int i = 0; i < result.Length; i++)
                {
                    Assert.AreEqual(expected[i].Id, result[i].Id);
                    Assert.AreEqual(expected[i].Name, result[i].Name);
                    Assert.AreEqual(expected[i].Description, result[i].Description);
                    Assert.AreEqual(expected[i].AuthorId, result[i].AuthorId);
                    Assert.AreEqual(expected[i].IndustryId, result[i].IndustryId);
                }
            }
        }
        [TestMethod]
        public async Task FilterReportsShould_Succeed_ByAll()
        {
            var options = InsightHubUtils.GetOptions();
            var tags = new Tag[]
            {
                 new Tag
                 {
                     Id = InsightHubUtils.B2W_Id,
                     Name = "B2W"
                 },
                 new Tag
                 {
                     Id = InsightHubUtils.D2_Id,
                     Name = "D2"
                 },
            };

            Report[] expected = new Report[]
                {
                new Report
                {
                    Id = InsightHubUtils.HowGoodIsAO2DE_Id,
                    Name = "WC3 is being saved from Pad!",
                    Description = "WC3 is being saved from Pad!",
                    AuthorId = InsightHubUtils.Yani_Id,
                    IndustryId = InsightHubUtils.Gaming_Id,
                    IsPending = true
                },
            };
            using (InsightHubContext context = new InsightHubContext(options))
            {
                InsightHubUtils.Seed(context);
            }
            using (InsightHubContext context = new InsightHubContext(options))
            {
                var sut = new FilterReportsService(new Services.Services.Main.ReportService(context, new FakeMap(),
                    InsightHubUtils.blobService, InsightHubUtils.mailer,
                    new ReportTagsService(context), null));
                var result = (await sut.FilterReportsAsync(null, tags.Select(x => x.Name).ToArray(), null)).ToArray();

                Assert.AreEqual(expected.Length, result.Length);
                for (int i = 0; i < result.Length; i++)
                {
                    Assert.AreEqual(expected[i].Id, result[i].Id);
                    Assert.AreEqual(expected[i].Name, result[i].Name);
                    Assert.AreEqual(expected[i].Description, result[i].Description);
                    Assert.AreEqual(expected[i].AuthorId, result[i].AuthorId);
                    Assert.AreEqual(expected[i].IndustryId, result[i].IndustryId);
                }
            }
        }
    }
}
