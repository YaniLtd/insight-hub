﻿using InsightHub.Data.DataAccessContext;
using InsightHub.Data.Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using InsightHub.Services.Services.Main;
using InsightHub.Services.Mappers.Contracts;
using InsightHub.Services.DTOs;
using System.Linq;
using InsightHub.Services.Services.Help;
using InsightHub.Services.Contracts;

namespace InsightHub.Test.ReportService
{
    [TestClass]
    public class AproveReportShould
    {
        private class FakeSubService : ISubscriptionService
        {
            public Task<bool> DeleteSubscriptionAsync(int? userId, int? industryId)
            {
                return default;
            }

            public Task<IEnumerable<IndustryDTO>> GetSubscriptionsOfUserAsync(int? userId)
            {
                return default;
            }
            public Task<IEnumerable<IndustryDTO>> GetUserNonSubscriptionsAsync(int? userId)
            {
                return default;
            }
            public Task<bool> HardDeleteSubscriptionAsync(int? userId, int? industryId)
            {
                return default;
            }
            public Task NotifySubscribers(int? industryId, ReportDTO report)
            {
                return default;
            }
            public Task SubscribeAsync(int? userId, int? industryId)
            {
                return default;
            }
        }
        private class FakeMap : IDTOMapper<Report, ReportDTO>
        {
            public ReportDTO MapFrom(Report entity)
            {
                return new ReportDTO
                {
                    Id = entity.Id,
                    Name = entity.Name,
                    Description = entity.Description,
                    AuthorId = entity.AuthorId,
                    IndustryId = entity.IndustryId,
                    IsPending = entity.IsPending
                };
            }

            public IEnumerable<ReportDTO> MapFrom(IEnumerable<Report> entities)
            {
                return entities.Select(x => MapFrom(x));
            }
        }
        [TestMethod]
        public async Task AproveReportShould_Succeed()
        {
            var options = InsightHubUtils.GetOptions();
            var expected = new ReportDTO
            {
                Id = InsightHubUtils.HowImportantIsVitaminA_Id,
                Name = "How important is vitamin A?",
                Description = "How important is vitamin A?",
                AuthorId = InsightHubUtils.Mephisto_Id,
                IndustryId = InsightHubUtils.HealthCare_Id,
                IsPending = true
            };

            using (InsightHubContext context = new InsightHubContext(options))
            {
                InsightHubUtils.Seed(context);
            }
            using (InsightHubContext context = new InsightHubContext(options))
            {
                var sut = new Services.Services.Main.ReportService(context, new FakeMap(),
                    InsightHubUtils.blobService, InsightHubUtils.mailer, 
                    new ReportTagsService(context), new FakeSubService());
                var result = (await sut.AproveReportAsync(expected.Id));

                Assert.AreEqual(expected.Name, result.Name);
                Assert.AreEqual(expected.Description, result.Description);
                Assert.AreEqual(expected.AuthorId, result.AuthorId);
                Assert.AreEqual(expected.IndustryId, result.IndustryId);
            }
        }
        [TestMethod]
        public async Task AproveReportShould_Throw_Deleted()
        {
            var options = InsightHubUtils.GetOptions();
            var deletedReportId = InsightHubUtils.HowImportantIsVitaminD_Id;

            using (InsightHubContext context = new InsightHubContext(options))
            {
                InsightHubUtils.Seed(context);
            }
            using (InsightHubContext context = new InsightHubContext(options))
            {
                var sut = new Services.Services.Main.ReportService(context, new FakeMap(),
                    InsightHubUtils.blobService, InsightHubUtils.mailer, 
                    new ReportTagsService(context), new FakeSubService());
                await Assert.ThrowsExceptionAsync<ArgumentException>
                    (async () => await sut.AproveReportAsync(deletedReportId));
            }
        }
        [TestMethod]
        public async Task AproveReportShould_Throw_Existing()
        {
            var options = InsightHubUtils.GetOptions();
            var deletedReportId = 999;

            using (InsightHubContext context = new InsightHubContext(options))
            {
                InsightHubUtils.Seed(context);
            }
            using (InsightHubContext context = new InsightHubContext(options))
            {
                var sut = new Services.Services.Main.ReportService(context, new FakeMap(),
                    InsightHubUtils.blobService, InsightHubUtils.mailer, 
                    new ReportTagsService(context), new FakeSubService());
                await Assert.ThrowsExceptionAsync<ArgumentException>
                    (async () => await sut.AproveReportAsync(deletedReportId));
            }
        }

    }
}
