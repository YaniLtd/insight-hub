﻿using InsightHub.Data.DataAccessContext;
using InsightHub.Data.Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using InsightHub.Services.Services.Main;
using InsightHub.Services.Mappers.Contracts;
using InsightHub.Services.DTOs;
using System.Linq;

namespace InsightHub.Test.UserService
{
    [TestClass]
    public class UpdateUserShould
    {
        private class FakeMap : IDTOMapper<User, UserDTO>
        {
            public UserDTO MapFrom(User entity)
            {
                return new UserDTO
                {
                    IsDeleted = entity.IsDeleted,
                    IsBanned = entity.LockoutEnd > DateTime.UtcNow,
                    Id = entity.Id,
                    Name = entity.Name,
                    Email = entity.Email
                };
            }

            public IEnumerable<UserDTO> MapFrom(IEnumerable<User> entities)
            {
                return entities.Select(x => MapFrom(x));
            }
        }
        [TestMethod]
        public async Task UpdateUserShould_Throw_NullArgument()
        {
            var options = InsightHubUtils.GetOptions();
            UserDTO user = null;

            using (InsightHubContext context = new InsightHubContext(options))
            {
                var sut = new Services.Services.Main.UserService(context, new FakeMap());
                await Assert.ThrowsExceptionAsync<ArgumentNullException>
                    (async () => await sut.UpdateUserAsync(user));
            }
        }
        [TestMethod]
        public async Task UpdateUserShould_Throw_Nonexisting()
        {
            var options = InsightHubUtils.GetOptions();
            var user = new UserDTO
            {
                Id = InsightHubUtils.Baal_Id,
                Name = "Baal Stoyanov",
                Email = "Baal_Id_1999@mail.bg",
                Password = "12345678"
            };


            using (InsightHubContext context = new InsightHubContext(options))
            {
                var sut = new Services.Services.Main.UserService(context, new FakeMap());
                await Assert.ThrowsExceptionAsync<ArgumentNullException>
                    (async () => await sut.UpdateUserAsync(user));
            }
        }
        [TestMethod]
        public async Task UpdateUserShould_Throw_Deleted()
        {
            var options = InsightHubUtils.GetOptions();
            var user = new UserDTO
            {
                Id = InsightHubUtils.Uther2_Id,
                Name = "Uther2_Id Stoyanov",
                Email = "Uther2_Id@mail.bg",
                Password = "12345678"
            };

            using (InsightHubContext context = new InsightHubContext(options))
            {
                InsightHubUtils.Seed(context);
            }
            using (InsightHubContext context = new InsightHubContext(options))
            {
                var sut = new Services.Services.Main.UserService(context, new FakeMap());
                await Assert.ThrowsExceptionAsync<ArgumentException>
                    (async () => await sut.UpdateUserAsync(user));
            }
        }
        [TestMethod]
        public async Task UpdateUserShould_Succeed_Existing()
        {
            var options = InsightHubUtils.GetOptions();
            var expected = new UserDTO
            {
                Id = InsightHubUtils.Baal_Id,
                Name = "Baal Stoyanov",
                Email = "Baal_Id_1999@mail.bg",
                Password = "12345678"
            };

            using (InsightHubContext context = new InsightHubContext(options))
            {
                InsightHubUtils.Seed(context);
            }
            using (InsightHubContext context = new InsightHubContext(options))
            {
                var sut = new Services.Services.Main.UserService(context, new FakeMap());
                var result = (await sut.UpdateUserAsync(expected));

                Assert.AreEqual(expected.Id, result.Id);
                Assert.AreEqual(expected.Name, result.Name);
                Assert.AreEqual(expected.Email, result.Email);
            }
        }

    }
}
