﻿using System.Collections.Generic;
using InsightHub.Services.DTOs;
using System.Threading.Tasks;

namespace InsightHub.Services.Contracts
{
    public interface IReportService
    {
        /// <summary>
        /// Sets the report's status to approved
        /// </summary>
        Task<ReportDTO> AproveReportAsync(int? id);
        /// <summary>
        /// Creates a report in the database
        /// </summary>
        Task<ReportDTO> CreateReportAsync(ReportDTO reportDTO);
        /// <summary>
        /// Soft deletes the report
        /// </summary>
        Task<bool> DeleteReportAsync(int? id);
        /// <summary>
        /// Downloads the report file from the blob storage
        /// </summary>
        Task<ReportDTO> DownloadReportAsync(int? downloaderId, int? reportId);
        /// <summary>
        /// Sets the report's status to featured (admin logic)
        /// </summary>
        Task<ReportDTO> FeatureReportAsync(int? id);
        /// <summary>
        /// Retrieves the reports from the database 
        /// </summary>
        Task<IEnumerable<ReportDTO>> GetAllReportsAsync();
        /// <summary>
        /// Retrieves the approved reports from the database 
        /// </summary>
        Task<IEnumerable<ReportDTO>> GetAprovedReportsAsync();
        /// <summary>
        /// Retrieves the reports downloaded by a user from the database 
        /// </summary>
        Task<IEnumerable<ReportDTO>> GetDownloadedReportsByUserAsync(int? downloaderId);
        /// <summary>
        /// Retrieves the featured reports from the database 
        /// </summary>
        Task<IEnumerable<ReportDTO>> GetFeaturedReportsAsync(int count = 3);
        /// <summary>
        /// Retrieves the latest reports from the database 
        /// </summary>
        Task<IEnumerable<ReportDTO>> GetLatestReportsAsync(int count = 7);
        /// <summary>
        /// Retrieves the pending reports from the database 
        /// </summary>
        Task<IEnumerable<ReportDTO>> GetPendingReportsAsync();
        /// <summary>
        /// Retrieves a pending report from the database 
        /// </summary>
        Task<ReportDTO> GetPendingReportAsync(int? id);
        /// <summary>
        /// Retrieves the reports written by a user from the database 
        /// </summary>
        Task<IEnumerable<ReportDTO>> GetReportsOfUserAsync(int? userId);
        /// <summary>
        /// Retrieves the reports with most downloads from the database 
        /// </summary>
        Task<IEnumerable<ReportDTO>> GetTopReportsAsync(int count = 7);
        /// <summary>
        /// Updates the report
        /// </summary>
        Task<ReportDTO> UpdateReportAsync(ReportDTO reportDTO);
        /// <summary>
        /// Retrieves an approved report from the database 
        /// </summary>
        Task<ReportDTO> GetApprovedReportAsync(int? id);
        Task<ReportDTO> GetReportAsync(int? id);
        Task<ReportDTO> StopFeaturingReportAsync(int? id);
        Task RejectReportAsync(int? id);
    }
}
