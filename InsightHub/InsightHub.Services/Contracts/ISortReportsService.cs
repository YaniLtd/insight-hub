﻿using System.Collections.Generic;
using InsightHub.Services.DTOs;
using System.Threading.Tasks;

namespace InsightHub.Services.Contracts
{
    public interface ISortReportsService
    {
        /// <summary>
        /// Sort options: name, name_desc, download (by downloads count), download_desc, uploadDateup, loadDate_desc
        /// </summary>
        Task<IEnumerable<ReportDTO>> SortReportsAsync(string sortOrder);
    }
}