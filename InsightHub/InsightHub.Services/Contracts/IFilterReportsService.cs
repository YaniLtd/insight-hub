﻿using System.Collections.Generic;
using InsightHub.Services.DTOs;
using System.Threading.Tasks;

namespace InsightHub.Services.Contracts
{
    public interface IFilterReportsService
    {
        /// <summary>
        /// Filters the reports by name, tagNames, industryName (supports filtering by more than one criteria)
        /// </summary>
        Task<IEnumerable<ReportDTO>> FilterReportsAsync(string name, string[] tagNames, string industryName);
    }
}