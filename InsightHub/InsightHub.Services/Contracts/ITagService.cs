﻿using InsightHub.Data.Entities;
using InsightHub.Services.DTOs;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace InsightHub.Services.Contracts
{
    public interface ITagService
    {
        /// <summary>
        /// Creates a tag
        /// </summary>
        Task<TagDTO> CreateTagAsync(TagDTO tagDTO);
        /// <summary>
        /// Soft deletes a tag
        /// </summary>
        Task<bool> DeleteTagAsync(int? Id);
        /// <summary>
        /// Retrieves the tags from the database 
        /// </summary>
        Task<IEnumerable<TagDTO>> GetAllTagsAsync();
        /// <summary>
        /// Retrieves a tag from the database 
        /// </summary>
        Task<TagDTO> GetTagAsync(int? Id);
        /// <summary>
        /// Updates a tag 
        /// </summary>
        Task<TagDTO> UpdateTagAsync(TagDTO tagDTO);
    }
}