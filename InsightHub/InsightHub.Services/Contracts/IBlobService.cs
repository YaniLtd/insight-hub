﻿using InsightHub.Data.Entities.Blob;
using System.Collections.Generic;
using InsightHub.Services.DTOs;
using System.Threading.Tasks;

namespace InsightHub.Services.Contracts
{
    public interface IBlobService
    {
        /// <summary>
        /// Uploads the content of the given report as a pdf doc onto the blob storage.
        /// A pdf file will be generated first in the pdf source folder
        /// </summary>
        public Task UploadReportAsync(ReportDTO reportDTO);
        /// <summary>
        /// Retrieves the pdf doc with the given name from the blob storage
        /// </summary>
        public Task<BlobInformation> GetBlobAsync(string name);
        /// <summary>
        /// Retrieves the pdf docs from the blob storage
        /// </summary>
        /// <returns>Their names</returns>
        public Task<IEnumerable<string>> ListBlobsAsync();

        //public Task UploadFileBlobAsync(string filePath, string fileName);
        
        /// <summary>
        /// Uploads the content of a given file onto the blob storage.
        /// </summary>
        public Task UploadContentBlobAsync(string content, string fileName);

        /// <summary>
        /// Deletes the given file from the blob
        /// </summary>
        public Task DeleteBlobAsync(string blobName);
    }
}
