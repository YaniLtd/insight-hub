﻿using System.Threading.Tasks;

namespace InsightHub.Services.Contracts
{
    public interface IMailer
    {
        /// <summary>
        /// Notifies that the user's email has been confirmed
        /// </summary>
        Task EmailConfirmed(string email);
        /// <summary>
        /// Notifies that the user's report has been approved and uploaded on the website
        /// </summary>
        Task ReportAproved(string email, string reportName);
        /// <summary>
        /// Notifies that the user's registration request has been approved
        /// </summary>
        Task UserAproved(string email);
        /// <summary>
        /// Notifies that the user's report needs edits in order to be uploaded on the website
        /// </summary>
        Task ReportRejected(string email, string reportName);
        /// <summary>
        /// Notifies that the user's registration request has been rejected
        /// </summary>
        Task UserRejected(string email);
        /// <summary>
        /// Sends an email
        /// </summary>
        Task SendEmailAsync(string email, string subject, string body);
    }
}
