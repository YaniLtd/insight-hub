﻿using InsightHub.Services.DTOs;
using System.Threading.Tasks;

namespace InsightHub.Services.Contracts
{
    public interface IAuthenticateUserService
    {
        /// <summary>
        /// Provides a token if the user's status is valid
        /// </summary>
        /// <param name="username">email of the user</param>
        /// <param name="password">password of the user</param>
        /// <returns>UserDTO with the generated token</returns>
        Task<UserDTO> AuthenticateAsync(string username, string password);
    }
}