﻿using InsightHub.Services.DTOs;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace InsightHub.Services.Contracts
{
    public interface IBanService
    {
        /// <summary>
        /// Bans a user
        /// </summary>
        /// <param name="banDTO">Contains the info of the ban</param>
        /// <returns></returns>
        Task CreateBanAsync(BanDTO banDTO);
        /// <summary>
        /// Retrieves the banned users from the database 
        /// </summary>
        /// <returns>The users as DTOs</returns>
        Task<IEnumerable<UserDTO>> GetAllBannedUsersAsync();
        /// <summary>
        /// Retrieves the ban history of a user from the database 
        /// </summary>
        /// <returns>The bans of the user as DTOs</returns>
        Task<IEnumerable<BanDTO>> GetBanHistoryAsync(int? userId);
        /// <summary>
        /// Checks if the user is banned
        /// </summary>
        Task<bool> IsBannedAsync(int? userId);
        /// <summary>
        /// Unbans a user
        /// </summary>
        Task RemoveBanAsync(int? userId);
    }
}