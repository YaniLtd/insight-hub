﻿using System.Threading.Tasks;

namespace InsightHub.Services.Contracts
{
    public interface IRoleService
    {
        Task AssignRoleAsync(string roleName, int userId);
    }
}