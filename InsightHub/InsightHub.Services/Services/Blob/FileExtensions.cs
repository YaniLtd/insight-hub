﻿using Microsoft.AspNetCore.StaticFiles;

namespace InsightHub.Services.Services.Blob
{
    /// <summary>
    /// Contains logic used to ease work around files
    /// </summary>
    public static class FileExtensions
    {
        private static readonly FileExtensionContentTypeProvider Provider = new FileExtensionContentTypeProvider();

        public static string GetContentType(this string fileName)
        {
            if (!Provider.TryGetContentType(fileName, out var contentType))
            {
                contentType = "application/octet-stream";
            }
            return contentType;
        }
    }
}
