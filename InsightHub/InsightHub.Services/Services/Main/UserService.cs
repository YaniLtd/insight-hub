﻿using InsightHub.Services.Mappers.Contracts;
using InsightHub.Data.DataAccessContext;
using InsightHub.Services.Contracts;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using InsightHub.Data.Entities;
using InsightHub.Services.DTOs;
using System.Threading.Tasks;
using System.Linq;
using System;
using Microsoft.EntityFrameworkCore.Query;
using System.Web;

namespace InsightHub.Services.Services.Main
{
    /// <summary>
    /// Contains logics required for working with users 
    /// </summary>
    public class UserService : IUserService
    {
        private readonly InsightHubContext context;
        private readonly IDTOMapper<User, UserDTO> dtoMapper;
        private readonly IMailer mailer;
        private readonly IRoleService roleService;

        public UserService(InsightHubContext context, IRoleService roleService,
            IDTOMapper<User, UserDTO> dtoMapper, IMailer mailer)
        {
            this.context = context ?? throw new ArgumentNullException(nameof(context));
            this.dtoMapper = dtoMapper ?? throw new ArgumentNullException(nameof(dtoMapper));
            this.roleService = roleService ?? throw new ArgumentNullException(nameof(roleService));
            this.mailer = mailer ?? throw new ArgumentNullException(nameof(mailer));
        }

        public async Task<UserDTO> CreateUserAsync(UserDTO userDTO)
        {
            if (userDTO == null)
            {
                throw new ArgumentNullException("User dto argument was null");
            }
            var user = await context.Users.FirstOrDefaultAsync(x => x.Id == userDTO.Id);

            if (user == null)
            {
                user = new User();
                user.PasswordHash = new PasswordHasher<User>().HashPassword(user, userDTO.Password);
                CopyInfo(user, userDTO);
                _ = await this.context.Users.AddAsync(user);
            }

            else if (user.IsDeleted == true)
            {
                CopyInfo(user, userDTO);
                user.IsDeleted = false;
            }

            else
            {
                throw new ArgumentException("User exists!");
            }
            SetFlagsToDefault(user);
            await this.context.SaveChangesAsync();
            user = await RetrieveUsersFromContext()
                .FirstOrDefaultAsync(x => x.Email == user.Email);
            await roleService.AssignRoleAsync(user.Role, user.Id);
            await SetRoleAsync(user);

            return dtoMapper.MapFrom(user);
        }

        public async Task<UserDTO> UpdateUserAsync(UserDTO userDTO)
        {
            if (userDTO == null)
            {
                throw new ArgumentNullException("User dto argument was null");
            }
            var user = await this.context.Users
                .FirstOrDefaultAsync(u => u.Id == userDTO.Id);

            if (user == null)
            {
                throw new ArgumentNullException("User was not found!");
            }
            if (user.IsDeleted == true)
            {
                throw new ArgumentException("User is deleted!");
            }
            CopyInfo(user, userDTO);
            SetFlagsToDefault(user);
            await SetRoleAsync(user);//in case of bad input
            _ = await this.context.SaveChangesAsync();
            user = await RetrieveUsersFromContext()
               .FirstOrDefaultAsync(x => x.Id == userDTO.Id);
            return this.dtoMapper.MapFrom(user);
        }

        public async Task<UserDTO> GetUserAsync(int? id)
        {
            if (id == null)
            {
                throw new ArgumentNullException("User id argument was null");
            }
            var user = await RetrieveUsersFromContext()
                .FirstOrDefaultAsync(x => x.Id == id);

            if (user == null)
            {
                throw new ArgumentNullException("User was not found!");
            }
            if (user.IsDeleted == true)
            {
                throw new ArgumentException("User is deleted!");
            }

            var mappedUsers = this.dtoMapper.MapFrom(user);
            return mappedUsers;
        }

        public async Task<IEnumerable<UserDTO>> GetAllValidUsersAsync()
        {
            var users = await RetrieveUsersFromContext()
                .Where(u => u.IsDeleted == false).ToArrayAsync();

            var userDTOs = users.Where(x => x.LockoutEnd == null || x.LockoutEnd < DateTime.UtcNow);

            var mappedUsers = this.dtoMapper.MapFrom(userDTOs);
            return mappedUsers;
        }

        public async Task<bool> DeleteUserAsync(int? id)
        {
            if (id == null)
            {
                throw new ArgumentNullException("User id argument was null");
            }
            var user = await this.context.Users
                .FirstOrDefaultAsync(u => u.Id == id);

            if (user == null)
            {
                return false;
            }

            user.IsDeleted = true;
            await this.context.SaveChangesAsync();
            return true;
        }

        public async Task<UserDTO> AproveUserAsync(int? id)
        {
            if (id == null)
            {
                throw new ArgumentNullException("User id argument was null");
            }
            var user = await RetrieveUsersFromContext()
               .FirstOrDefaultAsync(x => x.Id == id);
            if (user == null)
            {
                throw new ArgumentNullException("User was not found!");
            }
            if (user.IsDeleted == true)
            {
                throw new ArgumentException("User is deleted!");
            }
            if (!user.IsApprovedByAdmin)
            {
                user.IsApprovedByAdmin = true;
                await mailer.UserAproved(user.Email);
            }
            _ = await this.context.SaveChangesAsync();
            var mappedUsers = this.dtoMapper.MapFrom(user);
            return mappedUsers;
        }

        public async Task<IEnumerable<UserDTO>> GetAprovedUsersAsync()
        {
            var users = await RetrieveUsersFromContext()
                .Where(x => x.IsDeleted == false && x.IsApprovedByAdmin == true).ToArrayAsync();

            var mappedUsers = this.dtoMapper.MapFrom(users);
            return mappedUsers;
        }

        public async Task<IEnumerable<UserDTO>> GetPendingUsersAsync()
        {
            var users = await RetrieveUsersFromContext()
                .Where(x => x.IsDeleted == false && x.IsApprovedByAdmin == false).ToArrayAsync();

            var mappedUsers = this.dtoMapper.MapFrom(users);
            return mappedUsers;
        }
        public async Task<UserDTO> GetUserByTokenAsync(string token)
        {
            if (token == null)
            {
                throw new HttpException(401, "Access denied!");
            }
            var user = await RetrieveUsersFromContext()
           .FirstOrDefaultAsync(x => x.Token == token);

            if (user == null)
            {
                throw new HttpException(401, "Access denied!");
            }

            var mappedUsers = this.dtoMapper.MapFrom(user);
            return mappedUsers;
        }

        //public async Task<List<> GetUser(string email, string password)
        private void SetFlagsToDefault(User user)
        {
            user.IsApprovedByAdmin = false;
            user.IsDeleted = false;
        }
        private void CopyInfo(User user, UserDTO userDTO)
        {
            user.Id = userDTO.Id;
            user.IsDeleted = userDTO.IsDeleted;
            user.Name = userDTO.Name;
            user.Email = userDTO.Email;
            user.Role = userDTO.Role;
            user.Bans = userDTO.Bans;
            user.PasswordHash = new PasswordHasher<User>().HashPassword(user, userDTO.Password);
        }
        private IIncludableQueryable<User, Industry> RetrieveUsersFromContext()
        {
            return context.Users
           .Include(x => x.Bans)
           .Include(x => x.Reports)
           .Include(x => x.Industries)
           .Include(x => x.ReportDownloaders).ThenInclude(x => x.Report)
           .Include(x => x.Subscriptions).ThenInclude(x => x.Industry);
        }
        private async Task SetRoleAsync(User user)
        {
            var role = (await context.UserRoles.FirstAsync(x => x.UserId == user.Id));
            if (role == null)
            {
                throw new ArgumentException("Role not found");
            }
            switch (role.RoleId)
            {
                case 1: user.Role = "Admin"; break;
                case 2: user.Role = "Author"; break;
                case 3: user.Role = "Customer"; break;
                default:
                   throw new ArgumentException("Role not found");
            }
        }
    }
}
