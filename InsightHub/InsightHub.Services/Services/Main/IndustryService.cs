﻿using InsightHub.Services.Mappers.Contracts;
using InsightHub.Data.DataAccessContext;
using Microsoft.EntityFrameworkCore;
using InsightHub.Services.Contracts;
using System.Collections.Generic;
using InsightHub.Data.Entities;
using InsightHub.Services.DTOs;
using System.Threading.Tasks;
using System.Linq;
using System;

namespace InsightHub.Services.Services.Main
{
    /// <summary>
    /// Contains logics required for working with industries 
    /// </summary>
    public class IndustryService : IIndustryService
    {
        private readonly InsightHubContext context;
        private readonly IDTOMapper<Industry, IndustryDTO> dtoMapper;

        public IndustryService(InsightHubContext context, IDTOMapper<Industry, IndustryDTO> dtoMapper)
        {
            this.context = context ?? throw new ArgumentNullException(nameof(context));
            this.dtoMapper = dtoMapper ?? throw new ArgumentNullException(nameof(dtoMapper));
        }
        public async Task<IndustryDTO> GetIndusry(int? id)
        {
            if (id == null)
            {
                throw new ArgumentNullException("Industry id was null!");
            }

            var industry = await context.Industries
               .FirstOrDefaultAsync(x => x.Id == id);

            if (industry == null)
            {
                throw new ArgumentNullException("Industry was not found");
            }
            if (industry.IsDeleted)
            {
                throw new ArgumentException("Industry has been deleted");
            }
            return this.dtoMapper.MapFrom(industry);
        }
        public async Task<Industry> GetIndusryEntityAsync(int? id)
        {
            if (id == null)
            {
                throw new ArgumentNullException("Industry id was null!");
            }

            var industry = await context.Industries
               .FirstOrDefaultAsync(x => x.Id == id);

            if (industry == null)
            {
                throw new ArgumentNullException("Industry was not found");
            }
            if (industry.IsDeleted)
            {
                throw new ArgumentException("Industry has been deleted");
            }
            return industry;
        }
        public async Task<IEnumerable<IndustryDTO>> GetAllIndustries()
        {
            var Industry = await context.Industries
             .Where(x => x.IsDeleted == false)
             .ToArrayAsync();

            return this.dtoMapper.MapFrom(Industry);
        }
    }
}
