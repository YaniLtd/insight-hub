﻿using InsightHub.Services.Mappers.Contracts;
using InsightHub.Data.DataAccessContext;
using Microsoft.EntityFrameworkCore;
using InsightHub.Services.Contracts;
using System.Collections.Generic;
using InsightHub.Data.Entities;
using InsightHub.Services.DTOs;
using System.Threading.Tasks;
using System.Linq;
using System;

namespace InsightHub.Services.Services.Main
{
    /// <summary>
    /// Contains logics required for working with tags 
    /// </summary>
    public class TagService : ITagService
    {
        private readonly InsightHubContext context;
        private readonly IDTOMapper<Tag, TagDTO> dtoMapper;

        public TagService(InsightHubContext context, IDTOMapper<Tag, TagDTO> dtoMapper)
        {
            this.context = context ?? throw new ArgumentNullException(nameof(context));
            this.dtoMapper = dtoMapper ?? throw new ArgumentNullException(nameof(dtoMapper));
        }
 
        public async Task<TagDTO> CreateTagAsync(TagDTO tagDTO)
        {

            if (tagDTO == null)
            {
                throw new ArgumentNullException("Tag argument was null!");
            }
            var tag = await context.Tags
              .FirstOrDefaultAsync(x => x.Id == tagDTO.Id);
            if (tag == null)
            {
                tag = new Tag { Name = tagDTO.Name };
                _ = await this.context.Tags.AddAsync(tag);
            }
            else if (tag.IsDeleted)
            {
                tag.IsDeleted = false;
                tag.Name = tagDTO.Name;
            }
            else
            {
                throw new ArgumentNullException("Tag exists!");
            }

            var mappedTag = this.dtoMapper.MapFrom(tag);
            _ = await this.context.SaveChangesAsync();

            return mappedTag;
        }

        public async Task<TagDTO> UpdateTagAsync(TagDTO tagDTO)
        {
            if (tagDTO == null)
            {
                throw new ArgumentNullException("Tag argument was null!");
            }

            var tag = await context.Tags
               .FirstOrDefaultAsync(x => x.Id == tagDTO.Id);

            if (tag == null)
            {
                throw new ArgumentNullException("The tag was not found");
            }
            if (tag.IsDeleted)
            {
                throw new ArgumentNullException("The tag was deleted!");
            }

            tag.Name = tagDTO.Name;
            _ = await context.SaveChangesAsync();

            var newTagDTO = this.dtoMapper.MapFrom(tag);
            return newTagDTO;
        }

        public async Task<TagDTO> GetTagAsync(int? Id)
        {
            if (Id == null)
            {
                throw new ArgumentNullException("Id argument was null!");
            }

            var tag = await context.Tags
           .Include(x => x.Reports).ThenInclude(x => x.Report)
           .FirstOrDefaultAsync(x => x.Id == Id && x.IsDeleted == false);

            if (tag == null)
            {
                throw new ArgumentNullException("Tag name cannot be null!");
            }

            var mappedTags = this.dtoMapper.MapFrom(tag);
            return mappedTags;
        }

        public async Task<IEnumerable<TagDTO>> GetAllTagsAsync()
        {
            var tags = await context.Tags
            .Where(x => x.IsDeleted == false)
            .ToArrayAsync();

            var mappedTags = this.dtoMapper.MapFrom(tags);
            return mappedTags;
        }

        public async Task<bool> DeleteTagAsync(int? Id)
        {
            if (Id == null)
            {
                throw new ArgumentNullException("Id argument was null!");
            }
            var tag = await this.context.Tags
                  .FirstOrDefaultAsync(x => x.Id == Id);

            if (tag == null)
            {
                return false;
            }

            tag.IsDeleted = true;
            tag.DeletedOn = DateTime.UtcNow;
            await this.context.SaveChangesAsync();

            return tag.IsDeleted;
        }
    }
}
