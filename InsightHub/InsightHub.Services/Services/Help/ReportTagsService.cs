﻿using InsightHub.Data.DataAccessContext;
using InsightHub.Data.Entities;
using InsightHub.Services.Contracts;
using InsightHub.Services.DTOs;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace InsightHub.Services.Services.Help
{
    /// <summary>
    /// Contains logic for the relations between tags and reports
    /// </summary>
    public class ReportTagsService : IReportTagsService
    {
        private readonly InsightHubContext context;
        public ReportTagsService(InsightHubContext context)
        {
            this.context = context;
        }
        public async Task AssignTags(ReportDTO reportDTO)
        {
            if (reportDTO.TagsIds != null)
            {
                foreach (var tagId in reportDTO.TagsIds)
                {
                    ReportTags reportTag = await context.ReportTags
                        .FirstOrDefaultAsync(x => x.ReportId == reportDTO.Id && x.TagId.ToString() == tagId);
                    if (reportTag == null)
                    {
                        reportTag = new ReportTags
                        {
                            ReportId = reportDTO.Id,
                            TagId = int.Parse(tagId),
                        };
                        context.ReportTags.Add(reportTag);
                    }
                }
                await context.SaveChangesAsync();
            }
        }
    }
}