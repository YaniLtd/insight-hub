﻿using InsightHub.Data.DataAccessContext;
using InsightHub.Data.Entities;
using InsightHub.Services.Contracts;
using InsightHub.Services.DTOs;
using InsightHub.Services.JWTModels;
using InsightHub.Services.Mappers.Contracts;
using InsightHub.Services.Services.Main;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace InsightHub.Services.Services.Help
{
    /// <summary>
    /// Contains logic concerning the jwt authentication
    /// </summary>
    public class AuthenticateUserService : IAuthenticateUserService
    {
        private readonly InsightHubContext context;
        IDTOMapper<User, UserDTO> dtoUserMapper;
        private readonly SignInManager<User> signInManager;
        private readonly UserManager<User> userManager;
        private readonly AppSettings appSettings;
        public AuthenticateUserService(InsightHubContext context,
            IDTOMapper<User, UserDTO> dtoMapper, IOptions<AppSettings> appSettings,
            SignInManager<User> signInManager, UserManager<User> userManager)
        {
            this.context = context ?? throw new ArgumentNullException(nameof(context));
            this.dtoUserMapper = dtoMapper ?? throw new ArgumentNullException(nameof(dtoMapper));
            this.appSettings = appSettings.Value ?? throw new ArgumentNullException(nameof(appSettings));
            this.signInManager = signInManager ?? throw new ArgumentNullException(nameof(signInManager));
            this.userManager = userManager ?? throw new ArgumentNullException(nameof(userManager));
        }
        public async Task<UserDTO> AuthenticateAsync(string username, string password)
        {
            User user = userManager.Users.FirstOrDefault(x => x.Email == username);

            // return null if user not found
            if (user == null)
            {
                throw new Exception("Non-existing account!");
            }

            if (!user.IsApprovedByAdmin)
            {
                throw new Exception("Unapproved account!");
            }

            if (user.LockoutEnd > DateTime.UtcNow)
            {
                throw new Exception("Banned account!");
            }

            if (user.IsDeleted)
            {
                throw new Exception("Deleted account!");
            }

            var loginResult = await signInManager.PasswordSignInAsync(username, password, false, lockoutOnFailure: false);
            if (!loginResult.Succeeded)
            {
                throw new Exception("Email, password mismatch!");
            }

            GenerateToken(user);
            await context.SaveChangesAsync();

            return dtoUserMapper.MapFrom(user);
        }
        private void GenerateToken(User user) 
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(appSettings.Secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Name, user.Id.ToString())
                }),
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            user.Token = tokenHandler.WriteToken(token);
        }
    }
}
