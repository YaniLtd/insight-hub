﻿using InsightHub.Services.Contracts;
using System.Collections.Generic;
using InsightHub.Services.DTOs;
using System.Threading.Tasks;
using System.Linq;

namespace InsightHub.Services.Services.Help
{
    /// <summary>
    /// Contains sorting logic
    /// </summary>
    public class SortReportsService : ISortReportsService
    {
        private readonly IReportService reportService;
        private IEnumerable<ReportDTO> reports;

        public SortReportsService(IReportService reportService)
        {
            this.reportService = reportService;
        }

        public async Task<IEnumerable<ReportDTO>> SortReportsAsync(string sortOrder)
        {
            reports = await reportService.GetAllReportsAsync();

            reports = sortOrder switch
            {
                "name" => reports.OrderBy(b => b.Name),
                "name_desc" => reports.OrderByDescending(b => b.Name),
                "download" => reports.OrderBy(b => b.DownloadsCnt),
                "download_desc" => reports.OrderByDescending(b => b.DownloadsCnt),
                "uploadDate" => reports.OrderBy(b => b.UploadedOn),
                "uploadDate_desc" => reports.OrderByDescending(b => b.UploadedOn),
                _ => reports.OrderBy(b => b.Name),
            };
            return reports;
        }
    }
}
