﻿using InsightHub.Services.Mappers.Contracts;
using InsightHub.Data.DataAccessContext;
using Microsoft.EntityFrameworkCore;
using InsightHub.Services.Contracts;
using System.Collections.Generic;
using InsightHub.Data.Entities;
using InsightHub.Services.DTOs;
using System.Threading.Tasks;
using System.Linq;
using System;

namespace InsightHub.Services.Services.Help
{
    /// <summary>
    /// Contains logics for banning, unbanning and more
    /// </summary>
    public class BanService : IBanService
    {
        private readonly InsightHubContext context;
        private readonly IDTOMapper<User, UserDTO> dtoUserMapper;
        private readonly IDTOMapper<Ban, BanDTO> dtoBanMapper;
        public BanService(InsightHubContext context,
            IDTOMapper<User, UserDTO> dtoUserMapper, IDTOMapper<Ban, BanDTO> dtoBanMapper)
        {
            this.context = context ?? throw new ArgumentNullException(nameof(context));
            this.dtoUserMapper = dtoUserMapper ?? throw new ArgumentNullException(nameof(dtoUserMapper));
            this.dtoBanMapper = dtoBanMapper ?? throw new ArgumentNullException(nameof(dtoBanMapper));
        }
        public async Task<IEnumerable<UserDTO>> GetAllBannedUsersAsync()
        {
            var bannedUsers = await context.Users.Where(x => x.LockoutEnd > DateTime.UtcNow).ToArrayAsync();

            return dtoUserMapper.MapFrom(bannedUsers);
        }
        public async Task<bool> IsBannedAsync(int? userId)
        {
            if (userId == null)
            {
                throw new ArgumentNullException("User id argument was null");
            }
            var user = await this.context.Users
                .FirstOrDefaultAsync(x => x.Id == userId);

            return user.LockoutEnd > DateTime.UtcNow;
        }
        public async Task<IEnumerable<BanDTO>> GetBanHistoryAsync(int? userId)
        {
            if (userId == null)
            {
                throw new ArgumentNullException("User id argument was null");
            }
            var user = await this.context.Users
                .Include(u => u.Bans)
                .FirstOrDefaultAsync(x => x.Id == userId);

            var mappedBans = this.dtoBanMapper.MapFrom(user.Bans);
            return mappedBans;
        }
        public async Task CreateBanAsync(BanDTO banDTO)
        {
            if (banDTO == null)
            {
                throw new ArgumentNullException("Ban DTO given was null");
            }
            var user = await this.context.Users.FirstOrDefaultAsync(x => x.Id == banDTO.UserId);

            if (user == null)
            {
                throw new ArgumentException("User not found!");
            }

            var ban = new Ban()
            {
                Name = banDTO.Name,
                Description = banDTO.Description,
                UserId = banDTO.UserId,
                ExpiresOn = banDTO.ExpiresOn
            };

            user.LockoutEnabled = true;
            user.LockoutEnd = ban.ExpiresOn;
            await this.context.Bans.AddAsync(ban);
            await this.context.SaveChangesAsync();
        }
        public async Task RemoveBanAsync(int? userId)
        {
            if (userId == null)
            {
                throw new ArgumentNullException("User id argument was null");
            }
            var users = await this.context.Users
            .FirstOrDefaultAsync(x => x.Id == userId);

            users.LockoutEnd = DateTime.UtcNow;
            users.LockoutEnabled = false;

            await this.context.SaveChangesAsync();
        }
    }
}
