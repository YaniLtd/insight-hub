﻿using InsightHub.Services.Contracts;
using System.Collections.Generic;
using InsightHub.Services.DTOs;
using System.Threading.Tasks;
using System.Linq;
using System;

namespace InsightHub.Services.Services.Help
{
    /// <summary>
    /// Contains filtering logic
    /// </summary>
    public class FilterReportsService : IFilterReportsService
    {
        private readonly IReportService reportService;
        private IEnumerable<ReportDTO> reports;

        public FilterReportsService(IReportService reportService)
        {
            this.reportService = reportService;
        }

        public async Task<IEnumerable<ReportDTO>> FilterReportsAsync(string name, string[] tagNames, string industryName)
        {
            reports = await reportService.GetAllReportsAsync();

            if (name != null)
            {
                reports = FilterReportByName(name);
            }

            if (tagNames != null)
            {
                var res = new List<ReportDTO>();
                foreach (var report in reports)
                {
                    bool reportMatches = true;

                    foreach (var tagName in tagNames)
                    {
                        if (!report.TagNames.Contains(tagName))
                        {
                            reportMatches = false;
                            break;
                        }
                    }

                    if (reportMatches)
                    {
                        res.Add(report);
                    }
                }
                reports = res;

            }

            if (industryName != null)
            {
                reports = FilterReportByindustry(industryName);
            }

            return reports;
        }

        private IEnumerable<ReportDTO> FilterReportByName(string name)
        {
            return reports.Where(x => x.Name == name);
        }

        //private IEnumerable<ReportDTO> FilterReportByTags(string[] tagNames)
        //{
            
        //}

        private IEnumerable<ReportDTO> FilterReportByindustry(string industryName)
        {
            return reports.Where(x => x.IndustryName == industryName);
        }
    }
}