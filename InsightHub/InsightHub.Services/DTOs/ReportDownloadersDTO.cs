﻿using InsightHub.Data.Entities;

namespace InsightHub.Services.DTOs
{
    /// <summary>
    /// A model class that transfers data concerning relations between 
    /// the reports and their downloaders from the business services 
    /// to the potential consuming assemblies and vice versa
    /// </summary>
    public class ReportDownloadersDTO
    {
        public int ReportId { get; set; }
        public Report Report { get; set; }
        public int DownloaderId { get; set; }
        public User Downloader { get; set; }
    }
}
