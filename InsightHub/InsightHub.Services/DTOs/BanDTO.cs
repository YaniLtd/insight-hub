﻿using InsightHub.Data.Entities;
using System;

namespace InsightHub.Services.DTOs
{
    /// <summary>
    /// A model class that transfers Ban concerning data from the business services 
    /// to the potential consuming assemblies and vice versa
    /// </summary>
    public class BanDTO
    {
        public int UserId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime ExpiresOn { get; set; }
    }
}
