﻿namespace InsightHub.Services.DTOs
{
    /// <summary>
    /// A model class that transfers Industry concerning data from the business services 
    /// to the potential consuming assemblies and vice versa
    /// </summary>
    public class IndustryDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
