﻿using InsightHub.Data.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace InsightHub.Services.DTOs
{
    /// <summary>
    /// A model class that transfers Tag concerning data from the business services 
    /// to the potential consuming assemblies and vice versa
    /// </summary>
    public class TagDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public ICollection<ReportTags> Reports { get; set; }
        public override string ToString()
        {
            StringBuilder contentBuilder = new StringBuilder();
            contentBuilder.AppendLine($"Id: {this.Id}");
            contentBuilder.AppendLine($"Name: {this.Name}");
            return contentBuilder.ToString();
        }
    }
}
