﻿using InsightHub.Services.Mappers.Contracts;
using System.Collections.Generic;
using InsightHub.Data.Entities;
using InsightHub.Services.DTOs;
using System.Linq;

namespace InsightHub.Services.Mappers
{
    /// <summary>
    /// Used to map from ReportTags => ReportTagsDTO
    /// </summary>
    public class ReportTagsMapper : IDTOMapper<ReportTags, ReportTagsDTO>
    {
        public ReportTagsDTO MapFrom(ReportTags entity)
        {
            return new ReportTagsDTO
            {
                ReportId = entity.ReportId,
                Report = entity.Report,
                TagId = entity.TagId,
                Tag = entity.Tag
            };
        }

        public IEnumerable<ReportTagsDTO> MapFrom(IEnumerable<ReportTags> entities)
        {
            return entities.Select(x => MapFrom(x));
        }
    }
}
