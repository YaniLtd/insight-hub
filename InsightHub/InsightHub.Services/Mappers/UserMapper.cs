﻿using InsightHub.Services.Mappers.Contracts;
using System.Collections.Generic;
using InsightHub.Data.Entities;
using InsightHub.Services.DTOs;
using System.Linq;
using System;

namespace InsightHub.Services.Mappers
{
    /// <summary>
    /// Used to map from User => UserDTO
    /// </summary>
    public class UserMapper : IDTOMapper<User, UserDTO>
    {
        public UserDTO MapFrom(User entity)
        {
            _ = entity.Reports == null ?
                throw new ArgumentNullException("Reports not included") : true;
            _ = entity.Industries == null ?
                throw new ArgumentNullException("Industries not included") : true;
            _ = entity.ReportDownloaders == null ?
                throw new ArgumentNullException("ReportDownloaders not included") : true;
            _ = entity.Subscriptions == null ?
                throw new ArgumentNullException("Subscriptions not included") : true;
            return new UserDTO
            {
                Id = entity.Id,
                IsDeleted = entity.IsDeleted,
                IsApprovedByAdmin = entity.IsApprovedByAdmin,
                IsBanned = entity.LockoutEnd > DateTime.UtcNow,
                Name = entity.Name,
                Email = entity.Email,
                Bans = entity.Bans,
                Token = entity.Token,
                Role = entity.Role,
                PhoneNumber = entity.PhoneNumber,
                NamesOfWrittenReports = entity.Reports.Select(x => x.Name),
                NamesOfIndustries = entity.Industries.Select(x => x.Industry.Name),
                NamesOfDownloadedReports = entity.ReportDownloaders.Select(x => x.Report.Name),
                NamesOfSubscriptions = entity.Subscriptions.Select(x => x.Industry.Name)
            };
        }
        public IEnumerable<UserDTO> MapFrom(IEnumerable<User> entities)
        {
            return entities.Select(x => MapFrom(x));
        }
    }
}
