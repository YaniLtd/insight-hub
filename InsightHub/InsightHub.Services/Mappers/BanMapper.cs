﻿using System.Collections.Generic;
using InsightHub.Data.Entities;
using InsightHub.Services.DTOs;
using System.Linq;

namespace InsightHub.Services.Mappers.Contracts
{
    /// <summary>
    /// Used to map from Ban => BanDTO
    /// </summary>
    public class BanMapper : IDTOMapper<Ban, BanDTO>
    {
        public BanDTO MapFrom(Ban entity)
        {
            return new BanDTO
            {
                Name = entity.Name,
                Description = entity.Description,
                UserId = entity.UserId,
                ExpiresOn = entity.ExpiresOn,
            };
        }
        
        public IEnumerable<BanDTO> MapFrom(IEnumerable<Ban> entities)
        {
            return entities.Select(x => MapFrom(x));
        }
    }
}
