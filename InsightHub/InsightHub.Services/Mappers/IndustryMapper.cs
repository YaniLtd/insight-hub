﻿using InsightHub.Services.Mappers.Contracts;
using System.Collections.Generic;
using InsightHub.Data.Entities;
using InsightHub.Services.DTOs;
using System.Linq;

namespace InsightHub.Services.Mappers
{
    /// <summary>
    /// Used to map from Industry => IndustryDTO
    /// </summary>
    public class IndustryMapper : IDTOMapper<Industry, IndustryDTO>
    {
        public IndustryDTO MapFrom(Industry entity)
        {
            return new IndustryDTO
            {
                Id = entity.Id,
                Name = entity.Name
            };
        }

        public IEnumerable<IndustryDTO> MapFrom(IEnumerable<Industry> entities)
        {
            return entities.Select(x => MapFrom(x));
        }
    }
}
