﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InsightHub.Services.JWTModels
{
    /// <summary>
    /// A class that carries a secret used to generate jwt tokens
    /// </summary>
    public class AppSettings
    {
        public string Secret { get; set; }
    }
}
